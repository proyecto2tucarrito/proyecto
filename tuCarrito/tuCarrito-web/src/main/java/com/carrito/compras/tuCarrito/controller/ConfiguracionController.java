package com.carrito.compras.tuCarrito.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import com.carrito.compras.tuCarrito.ejb.EJBConfiguracion;
import com.carrito.compras.tuCarrito.ejb.EjbUsuario;
import com.carrito.compras.tuCarrito.ejb.IEjbUsuario;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Usuario;

@Named(value = "ConfiguracionController")
@SessionScoped
public class ConfiguracionController implements Serializable {

	@EJB
	private EJBConfiguracion ejbconfig;

	private UploadedFile archivoLogo;
	private UploadedFile archivoMobile;
	private String nombre;
	private String urlFront;
	private String imagenLogo;
	private String imagen_mobile;
	private String token_pasarela;
	private String key_push_firebase;
	private String secret_id_facebook;
	private String app_id;
	private int idUsuario;
	private UploadedFile archivo;
	private String contentTypeLogo;
	private String urlBack;
	private String email;
	
	private boolean habilitarBtnCrear;
	
	public void nextToStep2(){
		System.out.println("Entra en el nexttoStep2");
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			System.out.println("entra en el try");
			context.getExternalContext().redirect("configuracion2.xhtml");
			//Configuracion configSession = (Configuracion)context.getExternalContext().getSessionMap().get("crearempresa");
			//if(configSession != null){
				
			//	configSession.setImagen_logo(imagenLogo);
			//	configSession.setImagen_mobile(imagen_mobile);
				/*if(verticalSession.isEsTransporte()){
					verticalSession.setPreciokmv(precioKm);
				}
				else{
					verticalSession.setPrecioHorav(precioHora);
				}*/
				
				//context.getExternalContext().getSessionMap().put("crearempresa", configSession);
				//context.getExternalContext().redirect("configuracion3.xhtml");
			
		} catch (Exception e) {
			
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ocurrio un error");
	            FacesContext.getCurrentInstance().addMessage("crearConfiguracionForm2:archivoLogo", msg);
			
			
		}
	}
	
	public void nextToStep3(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		System.out.println("entra en nexttostep3");
		try {
			
			context.getExternalContext().redirect("configuracion3.xhtml");
		} catch (Exception e) {
			
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ocurrio un error");
	            FacesContext.getCurrentInstance().addMessage("crearVerticalForm2:comisionYuber", msg);
			
			
		}
		
	}
	
	
	public void crearEmpresa() {
		
		Configuracion config;
		
		IEjbUsuario ejbUsuario =new EjbUsuario();
		Boolean insertado=false;
		Usuario user= new Usuario();
		
		/*SE BUSCA EL PARAMETRO*/
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map<String,String> params = facesContext.getExternalContext().getRequestParameterMap();
		String parametroObtenido= (String) params.get("email");
		System.out.println(email);
		
		try {
			user=ejbUsuario.getUsuarioByEmail(email);
			System.out.println("entra en el try de crearempresa "+email);
			if(user !=null) {
				System.out.println("entra en el usuario distinto de null");
				if(archivoLogo!=null) {
					System.out.println("Entra en el imagen logo no es null");
					if(archivoMobile!=null) {
						config=new Configuracion(nombre, urlFront, "", "", token_pasarela, 
									key_push_firebase, secret_id_facebook, app_id, user.getId(), urlBack); 
						System.out.println("entra en el usuario distinto de null");

						habilitarBtnCrear = false;
						System.out.println("dadadsdsadsada");
						System.out.println(archivoLogo);
						System.out.println(archivoMobile.getContents().toString());
						ejbconfig.insertarConfig(config,archivoLogo.getContents(),archivoMobile.getContents());
						
						FacesMessage msgCreacion = new FacesMessage(FacesMessage.SEVERITY_INFO,  "Se ha creado la empresa", "Dirijase a " + urlBack + " para administrar su negocio");
						FacesContext.getCurrentInstance().addMessage("crearVerticalForm3:btnCrearVertical", msgCreacion);
						facesContext.getExternalContext().redirect("listEmpresas.xhtml");
					}else {
						FacesMessage msg = new FacesMessage();
						msg.setSeverity(FacesMessage.SEVERITY_ERROR);
						facesContext.addMessage("No se pudo crear la empresa", msg);
					}
				}else {
					FacesMessage msg = new FacesMessage();
					facesContext.addMessage("No se pudo cargar el usuario de email "+email,msg);
				 }
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			FacesMessage msg = new FacesMessage();
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage("Error al  crear la empresa", msg);

			e.printStackTrace();
		}

	}
	
	public EJBConfiguracion getiEjbVertical() {
		return ejbconfig;
	}

	public void setiEjbVertical(EJBConfiguracion ejbconfig) {
		this.ejbconfig = ejbconfig;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public UploadedFile getArchivo() {
		return archivo;
	}

	public void setArchivo(UploadedFile archivo) {
		this.archivo = archivo;
	}

	public String getImagenLogo() {
		return imagenLogo;
	}

	public void setImagenLogo(String imagenLogo) {
		this.imagenLogo = imagenLogo;
	}

	public String getContentTypeLogo() {
		return contentTypeLogo;
	}

	public void setContentTypeLogo(String contentTypeLogo) {
		this.contentTypeLogo = contentTypeLogo;
	}
	public EJBConfiguracion getEjbconfig() {
		return ejbconfig;
	}
	public void setEjbconfig(EJBConfiguracion ejbconfig) {
		this.ejbconfig = ejbconfig;
	}
	public String getUrlFront() {
		return urlFront;
	}
	public void setUrlFront(String urlFront) {
		this.urlFront = urlFront;
	}
	public String getImagen_mobile() {
		return imagen_mobile;
	}
	public void setImagen_mobile(String imagen_mobile) {
		this.imagen_mobile = imagen_mobile;
	}
	public String getToken_pasarela() {
		return token_pasarela;
	}
	public void setToken_pasarela(String token_pasarela) {
		this.token_pasarela = token_pasarela;
	}
	public String getKey_push_firebase() {
		return key_push_firebase;
	}
	public void setKey_push_firebase(String key_push_firebase) {
		this.key_push_firebase = key_push_firebase;
	}
	public String getSecret_id_facebook() {
		return secret_id_facebook;
	}
	public void setSecret_id_facebook(String secret_id_facebook) {
		this.secret_id_facebook = secret_id_facebook;
	}
	public String getApp_id() {
		return app_id;
	}
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUrlBack() {
		return urlBack;
	}
	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}

	public UploadedFile getArchivoLogo() {
		return archivoLogo;
	}

	public void setArchivoLogo(UploadedFile archivoLogo) {
		this.archivoLogo = archivoLogo;
	}

	public UploadedFile getArchivoMobile() {
		return archivoMobile;
	}

	public void setArchivoMobile(UploadedFile archivoMobile) {
		this.archivoMobile = archivoMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isHabilitarBtnCrear() {
		return habilitarBtnCrear;
	}

	public void setHabilitarBtnCrear(boolean habilitarBtnCrear) {
		this.habilitarBtnCrear = habilitarBtnCrear;
	}

	
	public void upload() {
		System.out.println(archivoLogo.getFileName());
	}
	
}