package com.carrito.compras.tuCarrito.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.carrito.compras.tuCarrito.ejb.EJBConfiguracion;
import com.carrito.compras.tuCarrito.ejb.EjbUsuario;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Usuario;


//@Named(value = "loginController")
//@SessionScoped
//@ViewScoped para hacer ajax

@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable{

	@EJB
	private EjbUsuario ejbUsuario;
	
	@EJB
	private EJBConfiguracion ejbconfig;
	
	private Usuario usuario;
	
	
	
	    private String username;
	 
	 
	    private String password;
	 
	 
	    public void setUsername(String name) {
	        this.username = name;
	    }
	 
	    public String getUsername() {
	        return username;
	    }
	 
	 
	    public String getPassword() {
	        return password;
	    }
	 
	 
	    public void setPassword(String password) {
	        this.password = password;
	    }
	 
	
//	/*public LoginController() {
//		
//		usuario = new Usuario();
//	}*/
//	

	
	@PostConstruct
	public void init(){
		usuario = new Usuario();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	

	public void iniciarSesion(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if(username != null && !username.isEmpty() &&  password != null){
		
		
		Usuario us = null;
		try{
			Usuario datos = new Usuario();
			datos.setEmail(username);
			datos.setPassword(password);
			us = ejbUsuario.iniciarSesion(username,password);
			if(us != null){
				if(us.getSuper_admin()){
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
					context.getExternalContext().redirect("listUsuarios.xhtml");
					
				}else{
					if(us.getActivo()){
						FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
						Configuracion config = ejbconfig.getConfiguracion(us);
						
						if(config != null){
							FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("empresa", config.getNombre());
							context.getExternalContext().redirect( "infoVertical.xhtml");

							//redireccion = "infoVertical";
						}else{
							context.getExternalContext().redirect("vertical.xhtml");
							//redireccion = "vertical";
						}
				
						
						
					}else{
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usted ha sido bloqueado"));
					}
				}
			}
			else{
				
				 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Datos incorrectos");
		            FacesContext.getCurrentInstance().addMessage("loginForm:password", msg);
			}
			
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error!"));
		}
		}
	}
	
	public void cerrarSesion(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

}

