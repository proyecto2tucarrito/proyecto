package com.carrito.compras.tuCarrito.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.carrito.compras.tuCarrito.ejb.EJBCategoria;
import com.carrito.compras.tuCarrito.ejb.EJBMarca;
import com.carrito.compras.tuCarrito.jpa.Categoria;
import com.carrito.compras.tuCarrito.jpa.Marca;

@Named(value = "MarcaController")
@SessionScoped
public class MarcaCategoriaController implements Serializable{

	@EJB
	private EJBMarca ejbmarca;
	
	@EJB 
	private EJBCategoria ejbcateg;
	
	private String nombreMarca;
	
	private String nombreCategoria;
	
	private int id;
	
	private String accion;
	public void insertarMarca() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		try {
			Marca marca=new Marca(nombreMarca);
			ejbmarca.insert(base, marca);
			FacesMessage msg2= new FacesMessage();
			facesContext.addMessage("Marca creada con exito!!", msg2);
			
			facesContext.getExternalContext().redirect("listMarcas.xhtml");
		}catch(Exception e) {
			FacesMessage msg = new FacesMessage();
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage("Error al  crear la marca", msg);
		}
	}

	public void insertarCategoria() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		try {
			Categoria categ=new Categoria(nombreCategoria);
			ejbcateg.insertarCategoria(categ, base);
			FacesMessage msg2= new FacesMessage();
			facesContext.addMessage("Categoria creada con exito!!", msg2);
			facesContext.getExternalContext().redirect("listCategorias.xhtml");
		}catch(Exception e) {
			FacesMessage msg = new FacesMessage();
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage("Error al  crear la categoria", msg);
		}
	}
	
	public List<Marca> getAllMarcas(){
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base =(String) facesContext.getExternalContext().getSessionMap().get("base");
		return ejbmarca.getAll(base);
	}
	
	public List<Categoria> getAllCategorias(){
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base =(String) facesContext.getExternalContext().getSessionMap().get("base");
		return ejbcateg.obtenerTodas(base);
	}
	
	public void editarMarca(int idMarca) {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base = (String) facesContext.getExternalContext().getSessionMap().get("base");
		System.out.println("Entra en el editar marca"+ idMarca);
		try {
			Marca marca = ejbmarca.getMarcaById(base, idMarca);
			nombreMarca=marca.getNombre();
			accion="e";
			facesContext.getExternalContext().redirect("marca.xhtml");
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No fue posible editar la marca", msg);
		}
	}
	public void editarCategoria(int idCateg) {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base = (String) facesContext.getExternalContext().getSessionMap().get("base");
		System.out.println("Entra en el editar categoria"+ idCateg);
		try {
			Categoria categoria= ejbcateg.obtenerCategoriaXID(idCateg, base);
			nombreCategoria=categoria.getNombre();
			facesContext.getExternalContext().redirect("categoria.xhtml");
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No fue posible editar la categoria", msg);
		}
	}
	
	public void modificarMarca() {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		Marca marca= ejbmarca.getMarcaByNombre(base, nombreMarca);
		try {
			ejbmarca.actualizar(base, marca);
			
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No se pudo modificar la marca", msg);
		}
	}
	
	public void modificarCategoria() {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		Categoria categoria= ejbcateg.obtenerCategoriaXNombre(nombreCategoria, base);
		try {
			ejbcateg.actualizar(base, categoria);
			
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No se pudo modificar la marca", msg);
		}
	}
	
	public String agregarMarca() {
		accion="a";
		System.out.println("Entra en la funcion agregar marca");
		nombreMarca="";
		return "marca.xhtml";
	}
	
	public String agregarCategoria() {
		System.out.println("Entra en la funcion agregar marca");
		nombreCategoria="";
		return "categoria.xhtml";
	}
	
	public void deleteMarca(int idMarca) {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base = (String)facesContext.getExternalContext().getSessionMap().get("base");
		try{
			Marca marca= ejbmarca.getMarcaById(base, idMarca);
			ejbmarca.borrar(base, marca);
		}catch(Exception e) {

			FacesMessage msg = new FacesMessage();
			facesContext.addMessage("No se pudo eliminar la marca", msg);
		}
	}
	
	public void deleteCategoria(int idCateg) {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base = (String)facesContext.getExternalContext().getSessionMap().get("base");
		try {
			Categoria categ= ejbcateg.obtenerCategoriaXID(idCateg, base);
			ejbcateg.borrar(base, categ);
		}catch(Exception e) {
			FacesMessage msg = new FacesMessage();
			facesContext.addMessage("No se pudo eliminar la categoria", msg);
		}
	}
	public EJBMarca getEjbmarca() {
		return ejbmarca;
	}

	public void setEjbmarca(EJBMarca ejbmarca) {
		this.ejbmarca = ejbmarca;
	}

	public EJBCategoria getEjbcateg() {
		return ejbcateg;
	}

	public void setEjbcateg(EJBCategoria ejbcateg) {
		this.ejbcateg = ejbcateg;
	}

	public String getNombreMarca() {
		return nombreMarca;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
}
