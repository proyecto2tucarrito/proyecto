package com.carrito.compras.tuCarrito.rest;


import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.carrito.compras.tuCarrito.ejb.IEJBCliente;
import com.carrito.compras.tuCarrito.jpa.Cliente;


@RequestScoped
@Path("cliente")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ClienteRS {

	@EJB
	private IEJBCliente ejbCliente;
	
	@POST
	@Path("registro/{locationURL}")
	public Response create(@PathParam("locationURL") final String locationURL, final Cliente cliente) {

		try {
			
			Cliente cli = ejbCliente.insert(locationURL, cliente);
				
			return Response.ok(cli).build();
			
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	@Path("login/{locationURL}/{email}/{password}")
	public Response login(@PathParam("locationURL") final String locationURL, @PathParam("email") final String email, @PathParam("password") final String password) {
		try {
			
			Cliente cli = ejbCliente.iniciarSesion(locationURL, email, password);
			if(cli != null) 
			{
				return Response.ok(cli).build();
			}
			else 
			{
				return Response.status(Status.UNAUTHORIZED).build();
			}
			
		}catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	@GET
	@Path("loginGoogle/{locationURL}/{email}")
	public Response loginGoogle(@PathParam("locationURL") final String locationURL, @PathParam("email") final String email) {
		try {
			
			Cliente cliente = ejbCliente.iniciarSesionGoogle(locationURL, email);
			if(cliente != null) 
			{
				return Response.ok(cliente).build();
			}
			else 
			{
				return Response.status(Status.UNAUTHORIZED).build();
			}
			
		}catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
	@PUT
	@Path("update/{locationURL}")
	public Response update(@PathParam("locationURL") final String locationURL, final Cliente cliente) {

		try {
			
			ejbCliente.update(locationURL, cliente);
				
			return Response.ok(cliente).build();
			
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	//Mobile
	
	@POST
	@Path("registroMobile/{vertical}")
	public Response createMobile(@PathParam("vertical") final String vertical, final Cliente cliente) {

		try {
			ejbCliente.insertMobile(vertical, cliente);
			return Response
					.ok(cliente)
					.build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	@Path("checkUserLogin/{vertical}/{email}")
	public Response findByEmail(@PathParam("vertical") final String vertical,@PathParam("email") final String email) {
		try {
			
			Cliente cliente = ejbCliente.checkUserLogin(vertical, email);
			
			if (cliente != null) {
				
				return Response.ok(cliente).build();
			} 
			else {
				
				return Response.status(Status.NOT_FOUND).build();
			}
			
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@PUT
	@Path("updateToken/{vertical}/{email}")
	public Response updateToken(@PathParam("vertical") final String vertical,@PathParam("email") final String email, final Cliente cli) {
		try {
			
			Cliente cliente = ejbCliente.updateToken(vertical, email, cli);
			
			if (cliente != null) {
				return Response.ok(cliente).build();
				 
			} else {
				return Response.status(Status.NOT_FOUND).build();
				
			}
			
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	/*@PUT
	@Path("update/{vertical}")
	public Response updateMobile(@PathParam("vertical") String vertical, final Cliente cliente) {
		
		try {
			ejbCliente.updateCliente(vertical, cliente);
			return Response.ok(cliente).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		
	}*/
	
	@PUT
	@Path("logoutMobile/{vertical}")
	public Response logoutMobile(@PathParam("vertical") String vertical, final Cliente cliente) {
		
		try {
			Cliente cli = ejbCliente.logoutMobile(vertical, cliente);
			
			if (cli != null) {
				return Response.ok(cli).build();
			} else {
				return Response.status(Status.NOT_FOUND).build();
			}
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
}