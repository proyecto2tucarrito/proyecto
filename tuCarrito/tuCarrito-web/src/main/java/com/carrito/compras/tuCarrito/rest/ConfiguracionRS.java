package com.carrito.compras.tuCarrito.rest;

import java.sql.Connection;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.carrito.compras.tuCarrito.ejb.IEJBConfiguracion;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.util.Conectar;

@RequestScoped
@Path("configuracion")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ConfiguracionRS {

	@EJB
	private IEJBConfiguracion ejbconfig;
	
	@GET
	@Path("configuracion/{locationURL}")
	public Response obtenerConfiguracion(@PathParam("locationURL") final String locationURL) {
		try {
			Configuracion config = ejbconfig.getConfiguracionUrlFront("tucarrito", locationURL);
			if(config != null) 
			{
				return Response.ok(config).build();
			}
			else 
			{
				return Response.status(Status.UNAUTHORIZED).build();
			}
			
		}catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
}
