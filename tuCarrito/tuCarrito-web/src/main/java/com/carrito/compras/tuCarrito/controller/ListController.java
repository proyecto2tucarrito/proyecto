package com.carrito.compras.tuCarrito.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.carrito.compras.tuCarrito.ejb.EJBConfiguracion;
import com.carrito.compras.tuCarrito.ejb.EjbUsuario;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Usuario;


import java.util.List;

@Named(value = "listController")
@SessionScoped
public class ListController implements Serializable{

	@EJB
	private EjbUsuario ejbUsuario;
	
	@EJB
	private EJBConfiguracion ejbconfig;
	
	private int id;
	private Date creationdate = new Date();
	private String email;
	private String lastname;
	private String name;
	private String password;
	private String tel;
	private String nombre;
	private String urlfront;
	private String urlback;
	
	public ListController(){}
	
	
	public List<Usuario> getAllUsuarios() throws Exception {
		return ejbUsuario.getAllUsuario();
	}
	
	public List<Configuracion> getAllConfigs()throws Exception{
		return ejbconfig.getAll();
	}
	public void delete(int idUsr) throws Exception {
		ejbUsuario.delete(idUsr);
	}
	
	public void bloquear(int idUsr) throws Exception {
		Usuario usr = new Usuario(name, lastname, email, password, false, false, tel);
		ejbUsuario.bloquear(usr);
	}
	
	public void editar(String locationURL) {
		try {
			Configuracion config= ejbconfig.getConfiguracionUrlFront("tucarrito", locationURL);
			nombre=config.getNombre();
			urlfront=config.getUrlFront();
			urlback=config.getUlrBack();
			FacesContext.getCurrentInstance().getExternalContext().redirect("configuracion.xhtml");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	/*public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
        	return new DefaultStreamedContent();
        }
        else {
        	 Usuario user = null;
        	 Configuracion config=null;
        try {
        	user = ejbUsuario.getUsuarioById(getId());
        	
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msgCreacion = new FacesMessage(FacesMessage.SEVERITY_INFO,  "Error", "");
			FacesContext.getCurrentInstance().addMessage(null, msgCreacion);
		}
      //  StreamedContent file = new DefaultStreamedContent(bytes, mimeType, filename);  
            return new DefaultStreamedContent(new ByteArrayInputStream(u));
        }
   }*/

	
}
