package com.carrito.compras.tuCarrito.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.carrito.compras.tuCarrito.ejb.EjbUsuario;
import com.carrito.compras.tuCarrito.ejb.IEjbUsuario;
import com.carrito.compras.tuCarrito.jpa.Usuario;

@RequestScoped
@Path("usuario")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class UsuarioRS {

	@EJB
	private IEjbUsuario ejbUsuario;
	
	@POST
	@Path("registro")
	public Response create(final Usuario usuario) {

		try {
			ejbUsuario.insert(usuario);
			return Response
					.ok(usuario)
					.build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	@Path("login/{usuario}/{password}")
	public Response findById(@PathParam("usuario") final String usuario,@PathParam("password") final String password) {
		try {
			Usuario user = ejbUsuario.iniciarSesion(usuario, password);
			if(user!=null) {
				return Response.ok().build();
			}
			else {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			
		}catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}
}
