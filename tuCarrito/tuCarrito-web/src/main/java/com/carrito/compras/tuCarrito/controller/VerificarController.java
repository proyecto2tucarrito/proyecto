package com.carrito.compras.tuCarrito.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Named;

import com.carrito.compras.tuCarrito.jpa.Usuario;


@Named(value = "verificarController")
@ManagedBean
@SessionScoped
public class VerificarController implements Serializable {

	
	public VerificarController(){
		
	}
	
	public void verificarSesionAdminGeneral(ComponentSystemEvent event){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
			
			
			
			if(us == null){
				context.getExternalContext().redirect("index.xhtml");
			}else{
				
				if(!us.getSuper_admin()){
					context.getExternalContext().redirect("permisos.xhtml");
				}
			}
		}catch(Exception e){
			
		}
		
	}
	
	public void verificarSesionAdminVertical(ComponentSystemEvent event){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
			
			if(us == null){
				context.getExternalContext().redirect("index.xhtml");
			}else{
				if(us.getSuper_admin()){
					context.getExternalContext().redirect("permisos.xhtml");
				}
			}
		}catch(Exception e){
			
		}
		
	}
	
	public void verificarSesion(ComponentSystemEvent event){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
			
			if(us != null){
				if(us.getSuper_admin()){
					context.getExternalContext().redirect("listUsuarios.xhtml");
				}else{
					context.getExternalContext().redirect("index.xhtml"); //cambiar
				}
				
			}
		}catch(Exception e){
			
		}
		
	}
	
	public int estadoSesion(){
		int retorno = 0;
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
			
			if(us != null){
				retorno = us.getId();
			}
		}catch(Exception e){
			
		}
		return retorno;
		
	}
	
	public String loginText(){
		String retorno = "";
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			Usuario us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
			
			if(us == null){
				retorno = "Login";
			}else{
				retorno = "Logueado como "+us.getEmail();
			}
		}catch(Exception e){
			
		}
		return retorno;
	}
	
	public boolean isBackOffice(){
		
		boolean retVal = false;
		try{
			FacesContext context = FacesContext.getCurrentInstance();
			
			retVal = (boolean)context.getExternalContext().getSessionMap().get("estucarrito");
			
		
		}catch(Exception e){
			return retVal;
		}
	
		return retVal;
	}
	
}
