package com.carrito.compras.tuCarrito.rest;

import java.sql.Connection;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.carrito.compras.tuCarrito.ejb.EJBProducto;
import com.carrito.compras.tuCarrito.ejb.IEJBProducto;
import com.carrito.compras.tuCarrito.jpa.Producto;
import com.carrito.compras.tuCarrito.util.Conectar;

@RequestScoped
@Path("producto")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ProductoRS {

	private IEJBProducto ejbprod = new EJBProducto();
	
	@GET
	@Path("obtener/{locationURL}")
	public Response obtenerProductos(@PathParam("locationURL") final String locationURL) {
		
		try {
			List<Producto> prods = ejbprod.obtenerProductos(locationURL);
	        GenericEntity<List<Producto>> list = new GenericEntity<List<Producto>>(prods) {};
	        return Response.ok(list).build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@GET
	public List<Producto> obtenerProdcutos(@QueryParam("start") Integer startPosition,@QueryParam("max") final Integer maxResult ){
		final List<Producto> prods=null;
		return prods;
	}
}
