package com.carrito.compras.tuCarrito.controller;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.carrito.compras.tuCarrito.ejb.IEJBCategoria;
import com.carrito.compras.tuCarrito.ejb.IEJBMarca;
import com.carrito.compras.tuCarrito.ejb.IEJBProducto;

import com.carrito.compras.tuCarrito.jpa.Categoria;

import com.carrito.compras.tuCarrito.jpa.ImagenesProducto;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.jpa.Producto;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.carrito.compras.tuCarrito.jpa.Moneda;

@Named(value = "ProductoController")
@SessionScoped
public class ProductoController implements Serializable{

	@EJB
	private IEJBProducto ejbprod;
	@EJB
	private IEJBMarca ejbmarca;
	@EJB
	private IEJBCategoria ejbcateg;
	
	private String nombre;
	private String descripcion;
	private double precio;
	private double descuento;
	private double precioConDcto;
	private int stock;
	private boolean activo;
	private int idMarca;
	private int idCategoria;
	private List<ImagenesProducto> imagenes;
	private UploadedFile imagen;
	private Moneda Moneda;
	private StreamedContent streamedContentImagen;
	private byte[] auxiliar;
	private String accion;
	
	public void nextToStep1() {
		FacesContext context=FacesContext.getCurrentInstance();
		try {
			context.getExternalContext().redirect("producto1.xhtml");
		}catch(Exception e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ocurrio un error");
            FacesContext.getCurrentInstance().addMessage("crearProductoForm:nombre", msg);
		}
	}
	

	public void nextToStep2(){
		System.out.println("Entra en el nexttoStep2");
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			System.out.println("entra en el try");
			context.getExternalContext().redirect("producto2.xhtml");
			//Configuracion configSession = (Configuracion)context.getExternalContext().getSessionMap().get("crearempresa");
			//if(configSession != null){
				
			//	configSession.setImagen_logo(imagenLogo);
			//	configSession.setImagen_mobile(imagen_mobile);
				/*if(verticalSession.isEsTransporte()){
					verticalSession.setPreciokmv(precioKm);
				}
				else{
					verticalSession.setPrecioHorav(precioHora);
				}*/
				
				//context.getExternalContext().getSessionMap().put("crearempresa", configSession);
				//context.getExternalContext().redirect("configuracion3.xhtml");
			
		} catch (Exception e) {
			
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ocurrio un error");
	            FacesContext.getCurrentInstance().addMessage("crearConfiguracionForm2:archivoLogo", msg);
			
			
		}
	}
	
	public void nextToStep3(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		System.out.println("entra en nexttostep3");
		try {
			
			context.getExternalContext().redirect("configuracion3.xhtml");
		} catch (Exception e) {
			
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Ocurrio un error");
	            FacesContext.getCurrentInstance().addMessage("crearVerticalForm2:comisionYuber", msg);
			
			
		}
		
	}
	
	
	public void crearProducto() {
		System.out.println("222222222");
		Producto producto;
		/*SE BUSCA EL PARAMETRO*/
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		
		
		if(descuento!=0.0) {
			precioConDcto=precio*(descuento/100);
		}else {
			precioConDcto=precio;
		}
		try {
			System.out.println(imagenes);
			if (!imagenes.isEmpty()) {
				System.out.println("entra en el if de la lista-- "+base);
				producto = new Producto(nombre,descripcion,precio,descuento,precioConDcto,stock,activo,null,null,imagenes,Moneda);
				System.out.println("entra   "+producto.getNombre());
				System.out.println("categoria"+idCategoria);
				System.out.println("marca"+idMarca);
				ejbprod.insertarProducto(producto,base,idMarca,idCategoria);
				facesContext.getExternalContext().redirect("lisProductos.xhtml");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			FacesMessage msg = new FacesMessage();
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage("Error al  crear la empresa", msg);

			e.printStackTrace();
		}

	}
	
	public List<Producto> getAllProductos() throws Exception{
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base =(String) facesContext.getExternalContext().getSessionMap().get("base");
		System.out.println("la base es "+base);
			return ejbprod.getAlls(base);
		
		
	}
	
	public void editarProducto(int idProd) {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base = (String) facesContext.getExternalContext().getSessionMap().get("base");
		System.out.println("Entra en el editar producto"+ idProd);
		System.out.println("la base es"+base);
		try {
			Producto prod = ejbprod.getProductoById(base, idProd);
			nombre=prod.getNombre();
			descripcion=prod.getDescripcion();
			descuento=prod.getDescuento();
			precio=prod.getPrecio();
			precioConDcto=prod.getPrecioConDcto();
			stock=prod.getStock();
			activo=prod.getActivo();
			idMarca = prod.getMarca().getId();
			Moneda=prod.getMoneda();
			idCategoria=prod.getCategoria().getId();
			imagenes=prod.getImagenes();
			
			accion="e";
			facesContext.getExternalContext().redirect("producto1.xhtml");
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No fue posible editar la marca", msg);
		}
	}
	
	public void agregarImagen(){
		ImagenesProducto archivo= new ImagenesProducto();
		
		Map<String,String> configuracion = new HashMap<String,String>(); 
		configuracion.put("cloud_name", "daxcmhdj9"); 
		configuracion.put("api_key", "832979375164611"); 
		configuracion.put("api_secret", "rq4FAAxR6Cf3vLOTbbmGYjXmZZA"); 
		Cloudinary cloudinary = new Cloudinary(configuracion);
		
		System.out.println("Se hixo la configuracion de la nube");
		//SE ARMA EL ARCHIVO imegenLogo
		ByteArrayInputStream bais = new ByteArrayInputStream(imagen.getContents());
		BufferedImage originalImage;
		try {
			originalImage = ImageIO.read(bais);
		
		int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
		
		BufferedImage resizeImagePng = resizeImage(originalImage, type,510,400);
		ImageIO.write(resizeImagePng, "jpg", new File("arquero_caballo.jpg"));
		
		System.out.println("Se hizo el archivo imagenlogo");
		
		File file = new File("arquero_caballo.jpg");
		
		Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.emptyMap());
				
		System.out.println("222222222");
		System.out.println((String)uploadResult.get("url"));
		archivo.setImagen((String)uploadResult.get("url"));
		System.out.println(archivo);
		if (imagenes==null)
		{
			imagenes=new ArrayList<ImagenesProducto>();
		}
		System.out.println(imagenes);
		imagenes.add(archivo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
		}


	public IEJBProducto getEjbprod() {
		return ejbprod;
	}


	public void setEjbprod(IEJBProducto ejbprod) {
		this.ejbprod = ejbprod;
	}


	public IEJBMarca getEjbmarca() {
		return ejbmarca;
	}


	public void setEjbmarca(IEJBMarca ejbmarca) {
		this.ejbmarca = ejbmarca;
	}


	public IEJBCategoria getEjbcateg() {
		return ejbcateg;
	}


	public void setEjbcateg(IEJBCategoria ejbcateg) {
		this.ejbcateg = ejbcateg;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getAccion() {
		return accion;
	}


	public void setAccion(String accion) {
		this.accion = accion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public double getDescuento() {
		return descuento;
	}


	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}


	public int getStock() {
		return stock;
	}


	public void setStock(int stock) {
		this.stock = stock;
	}


	public boolean isActivo() {
		return activo;
	}


	public void setActivo(boolean activo) {
		this.activo = activo;
	}


	public int getMarca() {
		return idMarca;
	}


	public void setMarca(int marca) {
		this.idMarca = marca;
	}


	public int getCategoria() {
		return idCategoria;
	}


	public void setCategoria(int categoria) {
		this.idCategoria = categoria;
	}


	public List<ImagenesProducto> getImagenes() {
		return imagenes;
	}


	public void setImagenes(List<ImagenesProducto> imagenes) {
		this.imagenes = imagenes;
	}


	public UploadedFile getImagen() {
		return imagen;
	}


	public void setImagen(UploadedFile imagen) {
		this.imagen = imagen;
	}


	public Moneda getMoneda() {
		return Moneda;
	}


	public void setMoneda(Moneda moneda) {
		Moneda = moneda;
	}


	public double getPrecioConDcto() {
		return precioConDcto;
	}


	public void setPrecioConDcto(double precioConDcto) {
		this.precioConDcto = precioConDcto;
	}
	
	public void manejarUploadedFile(FileUploadEvent event) {
	    UploadedFile uploadedFile = (UploadedFile)event.getFile();
	    
	    try {                    
	        this.setAuxiliar(uploadedFile.getContents());
	        streamedContentImagen = new DefaultStreamedContent(new    ByteArrayInputStream(this.getAuxiliar()));
	    } catch (Exception e) {
	        //log error
	    }
	}

	public String agregarProducto() {
		System.out.println("Entra en la funcion agregar marca");
		 nombre="";
		 descripcion="";
		 precio=0;
		 descuento=0;
		 precioConDcto=0;
		 stock=0;
		 activo=false;
		 idMarca=0;
		 idCategoria=0;
		 imagenes=null;
		 imagen=null;
		 Moneda=null;
		 accion="a";
		return "producto1.xhtml";
	}
	
	public void modificarProducto() {
		FacesContext facesContext= FacesContext.getCurrentInstance();
		String base=(String)facesContext.getExternalContext().getSessionMap().get("base");
		
		try {
			//ejbprod.actualizar(base, nombre);
			
		}catch(Exception e) {
			FacesMessage msg= new FacesMessage();
			facesContext.addMessage("No se pudo modificar la marca", msg);
		}
	}
	
	public StreamedContent getStreamedContentImagen() {
	    return streamedContentImagen;
	}

	public void setStreamedContentImagen(StreamedContent streamedContentImagen) {
	    this.streamedContentImagen = streamedContentImagen;
	}


	public byte[] getAuxiliar() {
		return auxiliar;
	}


	public void setAuxiliar(byte[] auxiliar) {
		this.auxiliar = auxiliar;
	}


	public int getIdMarca() {
		return idMarca;
	}


	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}


	public int getIdCategoria() {
		return idCategoria;
	}


	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	
	
	
}
