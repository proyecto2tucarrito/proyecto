package com.carrito.compras.tuCarrito.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.model.UploadedFile;

import com.carrito.compras.tuCarrito.ejb.EjbUsuario;
import com.carrito.compras.tuCarrito.jpa.Direccion;
import com.carrito.compras.tuCarrito.jpa.Usuario;

@Named(value = "registroController")
@SessionScoped
public class RegistroController implements Serializable {

	@EJB
	private EjbUsuario ejbUsuario;

	private int id;
	private String email;
	private String apellido;
	private String nombre;
	private String password;
	private UploadedFile archivo;
	private String emailusuario;
	private String contentTypeLogo;
	private String tel;

	public void insert() throws Exception {

		try {
			if (email != null && apellido != null && nombre != null) {
				Usuario usr = new Usuario();
				if (!ejbUsuario.existeUsuario(email)) {
					usr = new Usuario(nombre, apellido, email, password,false,true, tel);

					ejbUsuario.insert(usr);

					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("configuracion.xhtml?email=" + email);
				} else {
					FacesMessage msg2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "",
							"Ya existe un usuario con ese mail");
					FacesContext.getCurrentInstance().addMessage("registroForm:email", msg2);
				}
			}
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Se produjo un error. Intentelo luego");
			FacesContext.getCurrentInstance().addMessage("registroForm:password", msg);
		}

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public EjbUsuario getEjbUsuario() {
		return ejbUsuario;
	}

	public void setEjbUsuario(EjbUsuario ejbUsuario) {
		this.ejbUsuario = ejbUsuario;
	}

	public UploadedFile getArchivo() {
		return archivo;
	}

	public void setArchivo(UploadedFile archivo) {
		this.archivo = archivo;
	}

	public String getContentTypeLogo() {
		return contentTypeLogo;
	}

	public void setContentTypeLogo(String contentTypeLogo) {
		this.contentTypeLogo = contentTypeLogo;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

}
