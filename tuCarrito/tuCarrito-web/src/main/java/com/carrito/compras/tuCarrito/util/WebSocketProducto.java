package com.carrito.compras.tuCarrito.util;

import java.io.IOException;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.carrito.compras.tuCarrito.ejb.IEJBConfiguracion;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Producto;

@Stateful
@ServerEndpoint("/productoSocket/{locationURL}")
public class WebSocketProducto {

	//private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
	//private Map<String, Set<Session>> sessiones = new HashMap<>();
	
	
	/*@OnMessage
	public String onMessage(String message, Session session, @PathParam("client-id") String clientId) {
		try {
			JSONObject jObj = new JSONObject(message);
			System.out.println("received message from client " + clientId);
			for (Session s : peers) {
				try {
					s.getBasicRemote().sendText(message);
					System.out.println("send message to peer ");
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "message was received by socket mediator and processed: " + message;
	}

	@OnOpen
	public void onOpen(Session session, @PathParam("client-id") String clientId) {
		System.out.println("mediator: opened websocket channel for client " + clientId);
		peers.add(session);

		try {
			session.getBasicRemote().sendText("good to be in touch");
		} catch (IOException e) {
		}
	}

	@OnClose
	public void onClose(Session session, @PathParam("client-id") String clientId) {
		System.out.println("mediator: closed websocket channel for client " + clientId);
		peers.remove(session);
	}*/
	
	//@EJB
	//private CacheConfiguracion cacheConfiguracion;
	
	@EJB
	private IEJBConfiguracion ejbConfiguracion;
	
	//@EJB
	//private CacheProductos cacheProductos;
	
	private static Map<String, Set<Session>> map = new HashMap<>();
	private static Map<String, Set<Session>> peers = Collections.synchronizedMap(map);
	private static Set<Session> setSession;
	
	@OnOpen
	public void onOpen(Session session, @PathParam("locationURL") String locationURL) {
		String base = getNombreBaseFromURL(locationURL);
		setSession = peers.get(base);
		
		if(setSession != null) {
			setSession.add(session);
			peers.put(base, setSession);
		}
		else {
			setSession = Collections.synchronizedSet(new HashSet<Session>());
			setSession.add(session);
			peers.put(base, setSession);
		}
	}

	@OnClose
	public void onClose(Session session, @PathParam("locationURL") String locationURL) {
		String base = getNombreBaseFromURL(locationURL);
		setSession = peers.get(base);
		
		if(setSession != null) {
			setSession.remove(session);
			peers.put(base, setSession);
		}
	}
	
	
	/*try {
		JSONObject jObj = new JSONObject(message);
		
		for (Session s : peers) {
			try {
				s.getBasicRemote().sendText(message);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	} catch (JSONException e) {
		e.printStackTrace();
	}*/
	
	/*@OnMessage
	public void onMessage(String message, Session session, @PathParam("locationURL") String locationURL) {
		
		String base = getNombreBaseFromURL(locationURL);
		setSession = peers.get(base);
		
		for (Session s : setSession) {
			try {
				s.getBasicRemote().sendText(message);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}*/
	
	@OnMessage
	public void onMessage(String prodMsg, Session session, @PathParam("locationURL") String locationURL) {
		
		String base = getNombreBaseFromURL(locationURL);
		String[] parts = prodMsg.split(",");
		int idProd = Integer.parseInt(parts[0]);
		int cantProd = Integer.parseInt(parts[1]);
		
		//Producto prod = cacheProductos.modifyCantProdToListCache(base, idProd, cantProd);
		//Producto prod = cacheProductos.modifyProductToListCache(base, producto);
		
		Producto prod = new Producto();
		prod.setId(idProd);
		prod.setStock(cantProd);
		
		setSession = peers.get(base);
		
		String toSend = prod.getId()+","+prod.getStock();
		for (Session s : setSession) {
			try {
				s.getBasicRemote().sendText(toSend);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private String getNombreBaseFromURL(String locationURL)
	{
		String retorno = null;
		try 
		{
			Configuracion config = ejbConfiguracion.getConfiguracionUrlFront("tucarrito", locationURL);
			if(config != null) 
			{
				retorno = config.getNombre();
			}
		}
		catch(Exception e) {
			
		}
		return retorno;
		
	}
}