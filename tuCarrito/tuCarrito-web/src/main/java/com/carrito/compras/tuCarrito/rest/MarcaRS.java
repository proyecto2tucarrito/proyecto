package com.carrito.compras.tuCarrito.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.carrito.compras.tuCarrito.ejb.IEJBMarca;
import com.carrito.compras.tuCarrito.jpa.Marca;


@RequestScoped
@Path("marca")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class MarcaRS {

	@EJB
	private IEJBMarca ejbMarca;
	
	@GET
	@Path("obtener/{locationURL}")
	public Response obtenerMarcas(@PathParam("locationURL") final String locationURL) {
		try {
			
			List<Marca> marcas = ejbMarca.getAllMarcas(locationURL);
	        GenericEntity<List<Marca>> list = new GenericEntity<List<Marca>>(marcas) {};
	        return Response.ok(list).build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}