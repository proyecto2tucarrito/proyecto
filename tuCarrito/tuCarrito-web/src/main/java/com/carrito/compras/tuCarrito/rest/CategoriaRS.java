package com.carrito.compras.tuCarrito.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.carrito.compras.tuCarrito.ejb.EJBCategoria;
import com.carrito.compras.tuCarrito.ejb.IEJBCategoria;
import com.carrito.compras.tuCarrito.jpa.Categoria;


@RequestScoped
@Path("categoria")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class CategoriaRS {
	
	@EJB
	private IEJBCategoria ejbcateg;
	

	@GET
	@Path("obtener/{locationURL}")
	public Response obtenerCategorias(@PathParam("locationURL") final String locationURL) {
		try {
			
			List<Categoria> categorias = ejbcateg.getAllCategorias(locationURL);
	        GenericEntity<List<Categoria>> list = new GenericEntity<List<Categoria>>(categorias) {};
	        return Response.ok(list).build();
			
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
