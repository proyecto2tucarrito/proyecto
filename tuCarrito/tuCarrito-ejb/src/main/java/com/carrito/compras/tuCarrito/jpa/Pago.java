package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Pago
 *
 */
@Entity

public class Pago implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private Date fecha;
		
	@ManyToOne(fetch=FetchType.EAGER,cascade= {CascadeType.ALL})
	@JoinColumn(name="idTarjeta")
	private Tarjeta tarjeta;
	
	public Pago() {
		super();
	}

	public Pago(int id, Date fecha, double monto,Tarjeta tarjeta) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.tarjeta=tarjeta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}
   
	
	
}
