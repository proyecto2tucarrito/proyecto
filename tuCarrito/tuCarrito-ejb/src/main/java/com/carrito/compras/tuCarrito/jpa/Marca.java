package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity implementation class for Entity: Marca
 *
 */
@Entity
@Table(name="Marca")
@NamedQueries({
@NamedQuery(name="Marca.getMarcaById",query="SELECT m FROM Marca m WHERE m.id= :idmarca"),
@NamedQuery(name="Marca.getMarcaByNombre",query="SELECT m FROM Marca m WHERE m.nombre= :nombre"),
@NamedQuery(name="Marca.getAllMarcas",query="SELECT m FROM Marca m")
})
public class Marca implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String nombre;
	
	public Marca() {
		super();
	}

	public Marca( String nombre) {
		super();
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
   
	
}
