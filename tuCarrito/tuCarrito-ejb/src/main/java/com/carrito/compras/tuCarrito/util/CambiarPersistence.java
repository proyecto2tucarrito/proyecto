package com.carrito.compras.tuCarrito.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CambiarPersistence implements Serializable{

	private static final long serialVersionUID = 1L; 
	
	EntityManagerFactory emf=null;
	private String user="root";
	
	private String password="proyecto2017";
	
	private String base="tucarrito";
	
	private String url="jdbc:mysql://";
	
	private String host="localhost";
	
	private int port=3306;
	
	private String driver="com.mysql.jdbc.Driver";
	
	public EntityManager getEntityManager(String base) {
	    EntityManager em = null;
	    Map properties = new HashMap();
	    properties.put("javax.persistence.jdbc.driver", driver);
	    properties.put("javax.persistence.jdbc.url", url+host+":"+port+"/"+base+"?verifyServerCertificate=false&useSSL=false");
	    properties.put("javax.persistence.jdbc.user", user);
	    properties.put("javax.persistence.jdbc.password", password);
	    try {
	        emf = Persistence.createEntityManagerFactory("primary", properties);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	    return em = (EntityManager) emf.createEntityManager();
	}

	public CambiarPersistence() {
		super();
	}
	
	
}
