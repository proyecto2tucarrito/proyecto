package com.carrito.compras.tuCarrito.util;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;

import com.carrito.compras.tuCarrito.jpa.Cliente;
import com.carrito.compras.tuCarrito.jpa.Producto;

@Singleton
public class CacheProductos {

	// private Map<String, List<Producto>> productos = new HashMap<>();
	// private Map<String, List<Cliente>> clientes = new HashMap<>();

	private Map<String, List<Producto>> productos = new ConcurrentHashMap<>();
	private Map<String, List<Cliente>> clientes = new ConcurrentHashMap<>();

	public void addProductList(String base, List<Producto> list) {
		if (!productos.containsKey(base)) {
			productos.put(base, list);
		}
	}

	public List<Producto> getProductList(String base) {
		if (productos.containsKey(base)) {
			return productos.get(base);
		} else {
			return null;
		}
	}

	public Producto modifyProductToListCache(String base, Producto producto) {
		List<Producto> list = this.getProductList(base);
		int index = 0;
		int indexLoop = 0;
		if (list != null) {
			// Iterator<Producto> iterator = list.iterator();
			ListIterator<Producto> iter = list.listIterator();
			while (iter.hasNext()) {
				indexLoop = iter.nextIndex();
				Producto prod = (Producto) iter.next();

				if (prod.getId() == producto.getId()) {
					index = indexLoop;
				}
			}
			list.set(index, producto);
			this.productos.put(base, list);
		}

		return producto;
	}
	
	public Producto modifyCantProdToListCache(String base, int id, int cantidad) {
		List<Producto> list = this.getProductList(base);
		int index = 0;
		int indexLoop = 0;
		Producto producto = null;
		if(list != null) {
			//Iterator<Producto> iterator = list.iterator();
			ListIterator<Producto> iter = list.listIterator();
			while(iter.hasNext()){
				indexLoop = iter.nextIndex();
				Producto prod = (Producto)iter.next();
				
				if(prod.getId() == id) {
					index = indexLoop;
					producto = prod;
				}
			}
			
			if(producto != null) {
				producto.setStock(cantidad);
				list.set(index, producto);
				this.productos.put(base, list);
			}
			
		}
		
		return producto;
	}
}
