package com.carrito.compras.tuCarrito.ejb;

import java.sql.Connection;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carrito.compras.tuCarrito.dao.DaoCliente;
import com.carrito.compras.tuCarrito.dao.DaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.DaoUsuario;
import com.carrito.compras.tuCarrito.dao.IDaoCliente;
import com.carrito.compras.tuCarrito.dao.IDaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.IDaoUsuario;
import com.carrito.compras.tuCarrito.jpa.Cliente;

import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.jpa.Usuario;
import com.carrito.compras.tuCarrito.util.CacheConfiguracion;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;

@Stateless
@LocalBean
public class EJBCliente implements IEJBCliente {
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private CacheConfiguracion cacheConfiguracion;
	
	private IDaoCliente iDaoCliente = new DaoCliente();
	private IDaoConfiguracion iDaoConfiguracion = new DaoConfiguracion();
	private CambiarPersistence cp = new CambiarPersistence();
	
	@Override
	public Cliente insert(String locationURL, Cliente cliente) throws Exception {
		
		Cliente cli = null;
		
		try {
			
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				if(iDaoCliente.checkEmailExists(em, cliente.getMail()).isEmpty()) 
				{
					iDaoCliente.insertarCliente(em, cliente);
					cli = cliente;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		return cli;
	}
	
	@Override
	public void update(String locationURL, Cliente cliente) throws Exception {
		
		try 
		{
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				iDaoCliente.updateCliente(em, cliente);
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(String locationURL, Cliente cliente) throws Exception {
		
		try 
		{
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				iDaoCliente.deleteCliente(em, cliente);
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Cliente> getAllClientes(String locationURL) throws Exception {
		
		try 
		{
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				return iDaoCliente.getClientesAll(em);
			}
			else 
			{
				return null;
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Cliente iniciarSesion(String locationURL, String email,String password) throws Exception {
		
		Cliente cli  = null;
		try 
		{ 
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				List<Cliente> lista = iDaoCliente.loginCliente(em, email, password);
				if(!lista.isEmpty())
				{
					cli = lista.get(0);
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return cli;
	}
	
	@Override
	public Cliente iniciarSesionGoogle(String locationURL, String email) throws Exception {
		
		Cliente cli  = null;
		try 
		{ 
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				List<Cliente> lista = iDaoCliente.checkEmailExists(em, email);
				if(!lista.isEmpty())
				{
					cli = lista.get(0);
				}
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return cli;
	}
	
	/*@Override
	public Cliente getClienteByEmail(String locationURL, String email) throws Exception{
		
		Cliente cli = null;
		try 
		{ 
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				cli = iDaoCliente.getClienteByEmail(em, email);
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return cli;
	}*/
	
	//********* MOBILE ********//
	
	@Override
	public Cliente getClienteByEmail(String base, String email) throws Exception{
		
		Cliente cli = null;
		try 
		{ 
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				cli = iDaoCliente.getClienteByEmail(em, email);
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return cli;
	}
	
	@Override
	public void insertMobile(String base, Cliente cliente) throws Exception {
		
		try {
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				if(iDaoCliente.checkEmailExists(em, cliente.getMail()).isEmpty()) 
				{
					iDaoCliente.insertarCliente(em, cliente);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
	}
	
	@Override
	public Cliente checkUserLogin(String base, String email) throws Exception {
		
		Cliente cliente = null;
		try 
		{
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				cliente = iDaoCliente.getClienteByEmail(em, email);
				if (cliente != null) {
					cliente.setActivo(true);
					iDaoCliente.updateCliente(em, cliente);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cliente;
	}
	
	@Override
	public Cliente updateToken(String base, String email, Cliente cli) throws Exception {
		
		Cliente cliente = null;
		try 
		{
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				cliente = iDaoCliente.getClienteByEmail(em, email);
				if (cliente != null) {
					cliente.setIdFirebase(cli.getIdFirebase());
					cliente.setTokenPush(cli.getTokenPush());
					iDaoCliente.updateCliente(em, cliente);
					 
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cliente;
	}
	
	@Override
	public Cliente logoutMobile(String base, Cliente cliente) throws Exception {
		
		Cliente cli = null;
		try {
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				cli = iDaoCliente.getClienteByEmail(em, cliente.getMail());
				if (cli != null) {
					cli.setActivo(false);
					iDaoCliente.updateCliente(em, cli);
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
		return cli;
	}
	
	private String getNombreBaseFromURL(String locationURL)
	{
		String retorno = null;
		try 
		{
			byte[] byteLocation = Base64.getDecoder().decode(locationURL); 
			
			Configuracion config = cacheConfiguracion.getConfigFromCache(new String(byteLocation));
			if(config != null) 
			{
				retorno = config.getNombre();
			}
			else 
			{
				em.clear();
				em = cp.getEntityManager("tucarrito");
				Conectar conexion=new Conectar();
				conexion.ConectarBase("tucarrito");
				
				List<Configuracion> configuracion = iDaoConfiguracion.getConfiguracionUrlFront(em, new String(byteLocation));
				if(!configuracion.isEmpty()) 
				{
					config = configuracion.get(0);
					cacheConfiguracion.addConfigToCache(new String(byteLocation), config);
					retorno = config.getNombre();
				}
			}
		}
		catch(Exception e) {
			
		}
		return retorno;
		
	}
	

}