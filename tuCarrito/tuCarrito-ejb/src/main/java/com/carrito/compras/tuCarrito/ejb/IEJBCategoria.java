package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Categoria;

@Local
public interface IEJBCategoria {

	void insertarCategoria(Categoria categ, String base) throws Exception;

	Categoria obtenerCategoriaXNombre(String categnombre, String base);

	List<Categoria> obtenerTodas(String base);

	Categoria obtenerCategoriaXID(int id, String base);

	void actualizar(String base, Categoria categ);

	void borrar(String base, Categoria categ);

	List<Categoria> getAllCategorias(String locationURL) throws Exception;
		
	
}
