package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/**
 * Entity implementation class for Entity: Configuracion
 *
 */
@Entity
@NamedQueries({
@NamedQuery(name="Configuracion.getConfiguracion",query="SELECT c FROM Configuracion c WHERE c.nombre= :nombre"),
@NamedQuery(name="Configuracion.getAll",query="SELECT c FROM Configuracion c"),
@NamedQuery(name="COnfiguracion.getConfiguracionUrlFront",query="SELECT c FROM Configuracion c WHERE c.urlFront= :urlfront")
})
public class Configuracion implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String nombre;
	
	@NotNull
	@Length(max=255)
	private String urlFront;
	
	private String imagen_logo;
	
	private String imagen_mobile;
	
	@NotNull
	private String token_pasarela;
	
	@NotNull
	private String key_push_firebase;
	
	@NotNull
	private String secret_id_facebook;
	
	@NotNull
	private String app_id;
	
	@NotNull
	private int idUsuario;
	
	@NotNull
	private String ulrBack;
	
	
	public Configuracion() {
		super();
	}

	

	public Configuracion(String nombre, String urlFront, String imagen_logo, String imagen_mobile,
			String token_pasarela, String key_push_firebase, String secret_id_facebook, String app_id, int idUsuario,
			String ulrBack) {
		super();
		this.nombre = nombre;
		this.urlFront = urlFront;
		this.imagen_logo = imagen_logo;
		this.imagen_mobile = imagen_mobile;
		this.token_pasarela = token_pasarela;
		this.key_push_firebase = key_push_firebase;
		this.secret_id_facebook = secret_id_facebook;
		this.app_id = app_id;
		this.idUsuario = idUsuario;
		this.ulrBack = ulrBack;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrlFront() {
		return urlFront;
	}

	public void setUrlFront(String url) {
		this.urlFront = url;
	}

	public String getImagen_logo() {
		return imagen_logo;
	}

	public void setImagen_logo(String imagen_logo) {
		this.imagen_logo = imagen_logo;
	}

	public String getImagen_mobile() {
		return imagen_mobile;
	}

	public void setImagen_mobile(String imagen_mobile) {
		this.imagen_mobile = imagen_mobile;
	}

	public String getToken_pasarela() {
		return token_pasarela;
	}

	public void setToken_pasarela(String token_pasarela) {
		this.token_pasarela = token_pasarela;
	}

	public String getKey_push_firebase() {
		return key_push_firebase;
	}

	public void setKey_push_firebase(String key_push_firebase) {
		this.key_push_firebase = key_push_firebase;
	}

	public String getSecret_id_facebook() {
		return secret_id_facebook;
	}

	public void setSecret_id_facebook(String secret_id_facebook) {
		this.secret_id_facebook = secret_id_facebook;
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUlrBack() {
		return ulrBack;
	}

	public void setUlrBack(String ulrBack) {
		this.ulrBack = ulrBack;
	}

}
