package com.carrito.compras.tuCarrito.ejb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carrito.compras.tuCarrito.dao.DaoUsuario;
import com.carrito.compras.tuCarrito.dao.IDaoUsuario;
import com.carrito.compras.tuCarrito.jpa.Usuario;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;

@Stateless
@LocalBean
public class EjbUsuario implements IEjbUsuario {
	
	@PersistenceContext
	private EntityManager em = null;
	
	public EjbUsuario()
	{
	}
	
	@Override
	public void insert(Usuario usuario) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		Conectar conn= new Conectar();
		conn.ConectarBase("tucarrito");
		try {
			iDaoUsuario.insert(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Usuario getUsuarioById(int idUsr) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		try {
			return iDaoUsuario.getUsuarioById(em, idUsr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void update(Usuario usuario) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		try {
			iDaoUsuario.update(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int idUsr) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		try {
			iDaoUsuario.delete(em, idUsr);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void bloquear(Usuario usuario) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		try {
			iDaoUsuario.bloquear(em, usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Usuario> getAllUsuario() throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		
		try {
			return iDaoUsuario.getAllUsuario(em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Usuario iniciarSesion(String email,String password) throws Exception {
		IDaoUsuario iDaoUsuario = new DaoUsuario();
		CambiarPersistence nueva = new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		Conectar conn= new Conectar();
		Connection conexion=conn.ConectarBase("tucarrito");
		Usuario usuario = null;
		try { 
			List<Usuario> lista = iDaoUsuario.getUsuarioLogin(em, email, password);
			if(!lista.isEmpty())
			{
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
		return usuario;
	}
	
	@Override
	public Usuario getUsuarioByEmail(String email) {
		IDaoUsuario idausu =new DaoUsuario();
		CambiarPersistence nueva= new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		Conectar conn= new Conectar();
		Connection conexion=conn.ConectarBase("tucarrito");
		List<Usuario> users= idausu.getUsuarioByEmail(em, email);
		int sizelis=0;
		sizelis=users.size();
		
		if(sizelis==0) {
			return null;
		}else {
			return users.get(0);
		}
	}
	
	@Override
	public boolean existeUsuario(String email) {
		IDaoUsuario idausu =new DaoUsuario();
		CambiarPersistence nueva= new CambiarPersistence();
		em=nueva.getEntityManager("tucarrito");
		Conectar conn=new Conectar();
		conn.ConectarBase("tucarrito");
		boolean existe =idausu.existeUsuario(em, email);
		return existe;
	}

}

