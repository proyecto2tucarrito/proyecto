package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: LineasCompra
 *
 */
@Entity

public class LineasCompra implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	private int cantidad;
	
	@NotNull
	private double precio;
	
	@OneToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="idProducto")
	private Producto producto;
	
	@OneToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="idComentario")
	private Comentario comentario;
	
	public LineasCompra() {
		super();
	}

	public LineasCompra(int id, int cantidad, double precio, Producto productos,Comentario comentario) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.precio = precio;
		this.producto = productos;
		this.comentario=comentario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}
}
