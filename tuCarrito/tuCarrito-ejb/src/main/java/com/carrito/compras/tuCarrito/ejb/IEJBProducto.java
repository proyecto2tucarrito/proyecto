package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Producto;

@Local
public interface IEJBProducto {

public void insertarProducto(Producto prod,String base,int idMarca,int idCategoria) throws Exception;

public Producto obtenerProductoById(String locationURL, int idProd) throws Exception;

public List<Producto> obtenerProductos(String locationURL) throws Exception;

public void borrarProducto(Producto prod) throws Exception;

public void actualizarProducto(String base,Producto prod) throws Exception;

List<Producto> getAlls(String base)throws Exception;

Producto getProductoById(String base, int idProd) throws Exception;

}