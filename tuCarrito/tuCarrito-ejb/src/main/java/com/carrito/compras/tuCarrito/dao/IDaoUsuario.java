package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Usuario;

public interface IDaoUsuario {
	public void insert(EntityManager em, Usuario usuario) throws Exception;
	public Usuario getUsuarioById(EntityManager em, int idUsr) throws Exception;
	public void update(EntityManager em, Usuario usuario) throws Exception;
	public void delete(EntityManager em, int idUsr) throws Exception;
	public void bloquear(EntityManager em, Usuario usuario) throws Exception;
	public List<Usuario> getAllUsuario(EntityManager em) throws Exception;
	public List<Usuario> getUsuarioLogin(EntityManager em, String email, String pass) throws Exception;
	int getSuperAdmin(EntityManager em, int idUser);
	List<Usuario> getUsuarioByEmail(EntityManager em, String email);
	boolean existeUsuario(EntityManager em, String email);
	
}
