package com.carrito.compras.tuCarrito.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Categoria;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;
import com.mysql.jdbc.Connection;

public class DAOCategoria implements IDAOCategoria{
	
	
		
	@Override
	public Boolean insetarCategoria(EntityManager em, Categoria categ) {
		
		Boolean insertado=false;
		try {
			em.persist(categ);
			System.out.println("Se conecto");
			insertado=true;
		}
		catch(Exception e) {
			insertado=false;
		}
			return insertado;
	}
	
	@Override
	public Categoria buscarCategoriaPorNombre(EntityManager em,String categnombre) {
		Categoria categ=new Categoria();
		try {
			categ=(Categoria) em.createNamedQuery("Categoria.categoriaXnombre").setParameter("nombre", categnombre).getSingleResult();
			return categ;
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return categ;
	}
	
	@Override
	public List<Categoria> obtenerTodas(EntityManager em){
			List<Categoria> lcategs= new ArrayList<Categoria>();
			
			try {
				lcategs=em.createNamedQuery("Categoria.obtenerTodas").getResultList();
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			return lcategs;
	}
	
	@Override
	public Categoria obtenerCategoriaXId(EntityManager em,int id) {
		
		Categoria categ=new Categoria();
		try {
			categ= (Categoria) em.createNamedQuery("Categoria.categoriaXId").setParameter("id", id).getSingleResult();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return categ;
	}
	
	@Override
	public void actualizar(EntityManager em , Categoria categ) {
		em.merge(categ);
	}
	
	@Override
	public void borrar(EntityManager em , Categoria categ) {
		em.remove(obtenerCategoriaXId(em,categ.getId()));
	}
}
