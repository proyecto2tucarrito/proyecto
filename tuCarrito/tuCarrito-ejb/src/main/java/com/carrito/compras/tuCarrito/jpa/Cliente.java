package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity implementation class for Entity: Cliente
 *
 */
@Entity
@NamedQueries({
@NamedQuery(name="Cliente.getAll",query="SELECT c FROM Cliente c"),
@NamedQuery(name="Cliente.login",query="SELECT c FROM Cliente c WHERE c.mail= :email and c.password = :password"),
@NamedQuery(name="Cliente.exists",query="SELECT c FROM Cliente c WHERE c.mail= :email")
})
public class Cliente implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String nombre;
	
	@NotNull
	@Length(max=255)
	private String mail;
	
	@NotNull
	@Length(max=255)
	private String password;
	
	//private byte[] imagen;
	
	private String imagen;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="dirId")
	private Direccion direccion;
	
	private String tokenPush;
	
	private String idFirebase;
	
	private boolean activo;//Activo: 1 INACTIVO: 0
	
	public Cliente() {
		super();
	}

	public Cliente(int id, String nombre, String mail, String password, String imagen, Direccion direccion,
			String tokenPush, String idFirebase, boolean activo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.mail = mail;
		this.password = password;
		this.imagen = imagen;
		this.direccion = direccion;
		this.tokenPush = tokenPush;
		this.idFirebase = idFirebase;
		this.activo = activo;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public String getTokenPush() {
		return tokenPush;
	}

	public void setTokenPush(String tokenPush) {
		this.tokenPush = tokenPush;
	}

	public String getIdFirebase() {
		return idFirebase;
	}

	public void setIdFirebase(String idFirebase) {
		this.idFirebase = idFirebase;
	}



	public boolean getActivo() {
		return activo;
	}



	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	
	
}