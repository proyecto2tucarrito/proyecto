package com.carrito.compras.tuCarrito.ejb;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carrito.compras.tuCarrito.dao.DaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.IDaoConfiguracion;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Usuario;
import com.carrito.compras.tuCarrito.util.CacheConfiguracion;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

@Stateless
@LocalBean
public class EJBConfiguracion implements IEJBConfiguracion{

	@PersistenceContext
	private EntityManager em=null;
	
	@EJB
	private CacheConfiguracion cacheConfiguracion;
	
	private IDaoConfiguracion iconfig= new DaoConfiguracion();
	private CambiarPersistence cp = new CambiarPersistence();
	
	
	@Override
	public void insertarConfig(Configuracion config,byte[] imagenLogo,byte[] imagenMobile) throws Exception {
		try{
			//SE CONFIGURA LA CLOUD
			Map<String,String> configuracion = new HashMap<String,String>(); 
			configuracion.put("cloud_name", "daxcmhdj9"); 
			configuracion.put("api_key", "832979375164611"); 
			configuracion.put("api_secret", "rq4FAAxR6Cf3vLOTbbmGYjXmZZA"); 
			Cloudinary cloudinary = new Cloudinary(configuracion);
			
			//SE ARMA EL ARCHIVO imegenLogo
			ByteArrayInputStream bais = new ByteArrayInputStream(imagenLogo);
			BufferedImage originalImage = ImageIO.read(bais);
			int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			
			BufferedImage resizeImagePng = resizeImage(originalImage, type, 72, 72);
			ImageIO.write(resizeImagePng, "jpg", new File("arquero_caballo.jpg"));
			
			//SE ARMA EL ARCHIVO imagenMobile
			ByteArrayInputStream bais2 = new ByteArrayInputStream(imagenMobile);
			BufferedImage originalImage2 = ImageIO.read(bais2);
			int type2 = originalImage2.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage2.getType();
			
			BufferedImage resizeImagePng2 = resizeImage(originalImage2, type2, 72, 72);
			ImageIO.write(resizeImagePng2, "jpg", new File("arquero1.jpg"));
		
			File file = new File("arquero_caballo.jpg");
			File file2 = new File("arquero1.jpg");
			
			Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.emptyMap());
			Map uploadResult2 = cloudinary.uploader().upload(file2, ObjectUtils.emptyMap());
			
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager("tucarrito");
			Conectar conn= new Conectar();
			conn.ConectarBase("tucarrito");
			
			config.setImagen_logo((String)uploadResult.get("url"));
			config.setImagen_mobile((String)uploadResult2.get("url"));
			iconfig.insertarConfiguracion(em, config);
			conn.CrearBase(config.getNombre());
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public Configuracion getConfiguracion(Usuario usuario) throws Exception{
		int idUsuario;
		idUsuario=usuario.getId();
		Configuracion config= new Configuracion();
		try {
		List<Configuracion> configuracion=iconfig.getConfiguracion(em, idUsuario);
		if(!configuracion.isEmpty()) {
			config=configuracion.get(0);
		}
		}
		catch(Exception e) {
			throw e;
		}
		return config;
	}
	
	@Override
	public void updateConfig(Configuracion config) throws Exception {
		try {
			iconfig.updateConfiguracion(em, config);
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public Configuracion getConfiguracionUrlFront(String base, String locationURL) throws Exception{
		Configuracion config = null;
		try 
		{
			byte[] byteLocation = Base64.getDecoder().decode(locationURL); 
			
			config = cacheConfiguracion.getConfigFromCache(new String(byteLocation));
			if(config == null) 
			{
				em.clear();
				em = cp.getEntityManager("tucarrito");
				Conectar conexion=new Conectar();
				conexion.ConectarBase("tucarrito");
				
				List<Configuracion> configuracion = iconfig.getConfiguracionUrlFront(em, new String(byteLocation));
				if(!configuracion.isEmpty()) 
				{
					config = configuracion.get(0);
					cacheConfiguracion.addConfigToCache(new String(byteLocation), config);
				}
			}
		}
		catch(Exception e) {
			throw e;
		}
		return config;
	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
		}
	
	@Override
	public List<Configuracion> getAll()throws Exception{
		return iconfig.getAll(em);
	}
}
