package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@NamedQueries({
@NamedQuery(name="Usuario.login", query="SELECT u FROM Usuario u WHERE u.email = :email and u.password = :pass"),
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u WHERE u.super_admin = 0"),
@NamedQuery(name="Usuario.getUEmail",query="SELECT u FROM Usuario u WHERE u.email =:email")
})
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Length(max=255)
	private String nombre;

	@NotNull
	@Length(max=255)
	private String apellido;
	
	@NotNull
	@Length(max=255)
	private String email;
	
	@NotNull
	@Length(max=255)
	private String password;
	
	@NotNull
	private boolean super_admin;
	
	@NotNull
	private boolean activo;
	
	@NotNull
	private String telefono;
	
	
	public Usuario() {}
	
	public Usuario(String nombre, String apellido, String email, String password, boolean super_admin, boolean activo,String telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.password = password;
		this.super_admin = super_admin;
		this.activo = activo;
		this.telefono=telefono;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getSuper_admin() {
		return super_admin;
	}

	public void setSuper_admin(boolean super_admin) {
		this.super_admin = super_admin;
	}

	public boolean getActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	
}
