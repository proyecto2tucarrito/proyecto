package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.LineasCompra;

public interface IDaoLineasCompra {

	void insertLinea(EntityManager em, LineasCompra linea);

	List<LineasCompra> getLineasCompra(EntityManager em, int idCompra);

	void deleteLinea(EntityManager em, LineasCompra linea);

	void updateLineaCompra(EntityManager em, LineasCompra linea);

}
