package com.carrito.compras.tuCarrito.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Producto;

public interface IDaoProducto {

	void crearProducto(EntityManager em, Producto prod,int idCateg,int idMarca)throws Exception;

	List<Producto> getProductoById(EntityManager em, int id) throws Exception;

	List<Producto> getProductoAll(EntityManager em) throws Exception;

	void actualizarProducto(EntityManager em, Producto prod)throws Exception;

	void borrarProducto(EntityManager em, Producto prod)throws Exception;

	Producto getProductoXId(EntityManager em, int idProd)throws Exception;
}
