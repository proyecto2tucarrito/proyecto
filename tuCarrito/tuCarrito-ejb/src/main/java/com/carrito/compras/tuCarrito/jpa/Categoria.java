package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity implementation class for Entity: Categoria
 *
 */
@Entity
@Table(name="Categoria")
@NamedQueries({
@NamedQuery(name="Categoria.obtenerTodas",query="SELECT c FROM Categoria c"),
@NamedQuery(name="Categoria.categoriaXnombre",query="SELECT c FROM Categoria c WHERE c.nombre= :nombre"),
@NamedQuery(name="Categoria.categoriaXId",query="SELECT c FROM Categoria c WHERE c.id= :id")
})
public class Categoria implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String nombre;
	
	public Categoria() {
		super();
	}

	public Categoria(String nombre) {
		super();
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
   
	
}
