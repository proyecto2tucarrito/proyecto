package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Compra
 *
 */
@Entity
@NamedQueries({
@NamedQuery(name="Compra.getCompraCliente",query="SELECT c FROM Compra c WHERE c.id= :idCompra"),
@NamedQuery(name="Compra.getComprasCliente",query="SELECT c FROM Compra c WHERE c.cliente.id= :idCliente")
})
public class Compra implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private Date fecha_alta;
	
	@NotNull
	private double monto_total;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade= {CascadeType.ALL}) 
	@JoinColumn(name="idCliente")
	private Cliente cliente;
	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="idPago")
	private Pago pago;
	
	@Enumerated(EnumType.STRING)
	private Estado estado;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="idLinCompras")
	private List<LineasCompra> lineas_compra;
	
	public Compra() {
		super();
	}
   
}
