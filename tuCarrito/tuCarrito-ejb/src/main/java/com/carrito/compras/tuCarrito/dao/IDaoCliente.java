package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Cliente;

public interface IDaoCliente {

	void insertarCliente(EntityManager em, Cliente cliente) throws Exception;

	Cliente getClienteByEmail(EntityManager em, String email) throws Exception;

	void deleteCliente(EntityManager em, Cliente cliente) throws Exception;

	void updateCliente(EntityManager em, Cliente cliente) throws Exception;

	List<Cliente> getClientesAll(EntityManager em) throws Exception;

	List<Cliente> loginCliente(EntityManager em, String email, String password) throws Exception;
	
	List<Cliente> checkEmailExists(EntityManager em,String email)throws Exception;
}
