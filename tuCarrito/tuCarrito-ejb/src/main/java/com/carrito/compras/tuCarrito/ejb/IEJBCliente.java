package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Cliente;

@Local
public interface IEJBCliente {

	Cliente insert(String locationURL, Cliente cliente) throws Exception;

	Cliente iniciarSesion(String locationURL, String email, String password) throws Exception;

	Cliente iniciarSesionGoogle(String locationURL, String email) throws Exception;

	void update(String locationURL, Cliente cliente) throws Exception;

	void delete(String locationURL, Cliente cliente) throws Exception;

	List<Cliente> getAllClientes(String locationURL) throws Exception;

	Cliente getClienteByEmail(String base, String email) throws Exception;

	void insertMobile(String base, Cliente cliente) throws Exception;

	Cliente checkUserLogin(String base, String email) throws Exception;

	Cliente updateToken(String base, String email, Cliente cli) throws Exception;

	Cliente logoutMobile(String base, Cliente cliente) throws Exception;

}
