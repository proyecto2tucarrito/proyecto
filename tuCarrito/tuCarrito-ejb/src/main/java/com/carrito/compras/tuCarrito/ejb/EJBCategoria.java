package com.carrito.compras.tuCarrito.ejb;

import java.sql.Connection;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.context.spi.Context;
import javax.inject.Inject;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import com.carrito.compras.tuCarrito.dao.DAOCategoria;
import com.carrito.compras.tuCarrito.dao.DaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.IDAOCategoria;
import com.carrito.compras.tuCarrito.dao.IDaoConfiguracion;
import com.carrito.compras.tuCarrito.jpa.Categoria;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.util.CacheConfiguracion;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;


@Stateless
@LocalBean
public class EJBCategoria implements IEJBCategoria{

	@PersistenceContext
	public EntityManager emp;
	
	private CambiarPersistence cp=new CambiarPersistence();
	
	@EJB
	private CacheConfiguracion cacheConfiguracion;
	
	@Override
	public void insertarCategoria(Categoria categ,String base) throws Exception{
		
		System.out.println("Entra en el insertar del EJB");				 
		 IDAOCategoria idaocateg= new DAOCategoria();
		 System.out.println("pasa la nueva config");
		 CambiarPersistence nueva = new CambiarPersistence();
			emp=nueva.getEntityManager(base);
			Connection conn = new Conectar().ConectarBase(base);
		try {
			idaocateg.insetarCategoria(emp, categ);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Categoria obtenerCategoriaXNombre(String categnombre,String base) {
		IDAOCategoria idaocateg=new DAOCategoria();
		emp =new CambiarPersistence().getEntityManager(base);
		Connection conn = new Conectar().ConectarBase(base);
		try {
			return idaocateg.buscarCategoriaPorNombre(emp, categnombre);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Categoria> obtenerTodas(String base){
		IDAOCategoria idacateg=new DAOCategoria();
		emp=new CambiarPersistence().getEntityManager(base);
		Connection conn = new Conectar().ConectarBase(base);
		try {
			return idacateg.obtenerTodas(emp);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Categoria obtenerCategoriaXID(int id,String base) {
		IDAOCategoria idaocateg= new DAOCategoria();
		emp = new CambiarPersistence().getEntityManager(base);
		Connection conn = new Conectar().ConectarBase(base);
		return idaocateg.obtenerCategoriaXId(emp, id);
	}
	
	@Override
	public void actualizar(String base, Categoria categ) {
		IDAOCategoria idaocateg= new DAOCategoria();
		emp = new CambiarPersistence().getEntityManager(base);
		Connection conn = new Conectar().ConectarBase(base);
		idaocateg.actualizar(emp, categ);
	}
	
	@Override
	public void borrar(String base, Categoria categ) {
		EntityManager em = new CambiarPersistence().getEntityManager(base);
		IDAOCategoria idaocateg= new DAOCategoria();
		idaocateg.borrar(em, categ);
	}
	
	@Override
	public List<Categoria> getAllCategorias(String locationURL) throws Exception{
		try{
			IDAOCategoria idaocateg= new DAOCategoria();
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				emp.clear();
				emp = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				return idaocateg.obtenerTodas(emp);
			}
			else
			{
				return null;
			}
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	private String getNombreBaseFromURL(String locationURL)
	{
		String retorno = null;
		try 
		{
			byte[] byteLocation = Base64.getDecoder().decode(locationURL); 
			
			Configuracion config = cacheConfiguracion.getConfigFromCache(new String(byteLocation));
			if(config != null) 
			{
				retorno = config.getNombre();
			}
			else 
			{
				emp.clear();
				emp = cp.getEntityManager("tucarrito");
				Conectar conexion=new Conectar();
				conexion.ConectarBase("tucarrito");
				IDaoConfiguracion iDaoConfiguracion= new DaoConfiguracion();
				List<Configuracion> configuracion = iDaoConfiguracion.getConfiguracionUrlFront(emp, new String(byteLocation));
				if(!configuracion.isEmpty()) 
				{
					config = configuracion.get(0);
					cacheConfiguracion.addConfigToCache(new String(byteLocation), config);
					retorno = config.getNombre();
				}
			}
		}
		catch(Exception e) {
			
		}
		return retorno;
		
	}
}
