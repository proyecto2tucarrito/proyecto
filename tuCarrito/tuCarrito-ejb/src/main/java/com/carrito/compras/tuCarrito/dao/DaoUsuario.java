package com.carrito.compras.tuCarrito.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Usuario;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;

public class DaoUsuario implements IDaoUsuario {

	public DaoUsuario(){
		
	}
	
	@Override
	public void insert(EntityManager em, Usuario usuario) throws Exception {
		em.persist(usuario);
	}

	@Override
	public Usuario getUsuarioById(EntityManager em, int idUsr) throws Exception {
		return em.find(Usuario.class, idUsr);
	}

	@Override
	public void update(EntityManager em, Usuario usuario) throws Exception {
		em.merge(usuario);
	}

	@Override
	public void delete(EntityManager em, int idUsr) throws Exception {
		em.remove(getUsuarioById(em, idUsr));
	}
	
	@Override
	public void bloquear(EntityManager em, Usuario usuario) throws Exception {
		em.merge(usuario);
	}
	
	@Override
	public List<Usuario> getAllUsuario(EntityManager em) throws Exception {
		return em.createNamedQuery("Usuario.findAll",Usuario.class).getResultList();
	}
	
	@Override
	public List<Usuario> getUsuarioLogin(EntityManager em, String email, String pass) throws Exception {
		return em.createNamedQuery("Usuario.login",Usuario.class)
					.setParameter("email", email)
					.setParameter("pass", pass)
					.getResultList();
	}

	
	@Override
	public int getSuperAdmin(EntityManager em, int idUser) {
		
		return em.createNamedQuery("Usuario.getSuperAdmin",Usuario.class)
				.setParameter("idUsuario", idUser)
				.getFirstResult();
	}
	
	@Override
	public List<Usuario> getUsuarioByEmail(EntityManager em,String email) {
		return em.createNamedQuery("Usuario.getUEmail",Usuario.class).setParameter("email", email).getResultList();
	}
	
	@Override
	public boolean existeUsuario(EntityManager em,String email) {
		boolean existe = false;
		List<Usuario> lusuarios =new ArrayList<Usuario>();
		lusuarios=em.createNamedQuery("Usuario.findAll",Usuario.class).getResultList();
		Iterator ite = lusuarios.iterator();
		
		while(ite.hasNext()) {
			Usuario user=(Usuario) ite.next();
			if(user.getEmail().equals(email)) {
				existe=true;
			}
		}
		return existe;
	}
}
