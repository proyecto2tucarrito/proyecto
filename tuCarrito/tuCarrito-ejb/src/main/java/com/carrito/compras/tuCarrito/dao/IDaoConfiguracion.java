package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Configuracion;

public interface IDaoConfiguracion {

	void insertarConfiguracion(EntityManager em, Configuracion config) throws Exception;

	List<Configuracion> getConfiguracion(EntityManager em, int idUsuario) throws Exception;

	void updateConfiguracion(EntityManager em, Configuracion config) throws Exception;

	List<Configuracion> getConfiguracionUrlFront(EntityManager em, String url) throws Exception;

	List<Configuracion> getAll(EntityManager em) throws Exception;
}
