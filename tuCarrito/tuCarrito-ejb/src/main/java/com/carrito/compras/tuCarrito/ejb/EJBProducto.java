package com.carrito.compras.tuCarrito.ejb;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carrito.compras.tuCarrito.dao.DaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.DaoProducto;
import com.carrito.compras.tuCarrito.dao.IDaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.IDaoProducto;
import com.carrito.compras.tuCarrito.jpa.Producto;
import com.carrito.compras.tuCarrito.jpa.Categoria;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.util.CacheConfiguracion;
import com.carrito.compras.tuCarrito.util.CacheProductos;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;

@Stateful
@LocalBean
public class EJBProducto implements IEJBProducto {

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private CacheConfiguracion cacheConfiguracion;
	
	@EJB
	private CacheProductos cacheProductos;
	
	@EJB
	private IEJBMarca ejbmarca;
	
	@EJB
	private IEJBCategoria ejbcateg;
	
	private CambiarPersistence cp = new CambiarPersistence();
	
	private IDaoConfiguracion iDaoConfiguracion = new DaoConfiguracion();
	private IDaoProducto idaop = new DaoProducto();
	
	@Override
	public void insertarProducto(Producto prod,String base,int idMarca, int idCategoria) throws Exception{
		System.out.println("categoria"+idCategoria);
		System.out.println("marca"+idMarca);
		try {
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
			Connection conn=new Conectar().ConectarBase(base);
			
			/*Marca mar=ejbmarca.getMarcaById(base, idMarca);
			Categoria categ=ejbcateg.obtenerCategoriaXID(idCategoria, base);
			System.out.println("categoria"+categ.getNombre());
			System.out.println("marca"+mar.getNombre());
			prod.setCategoria(categ);
			prod.setMarca(mar);*/
			idaop.crearProducto(em, prod,idCategoria,idMarca);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Producto obtenerProductoById(String locationURL, int idProd) throws Exception{
		
		Producto prod = null;
		try 
		{
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				List<Producto> list = idaop.getProductoById(em, idProd);
				if(!list.isEmpty()) {
					prod = list.get(0);
				}
			}
		}
		catch(Exception e) {
			throw e;
		}
		
		return prod;
	}
	
	@Override 
	public List<Producto> getAlls(String base) throws Exception{
		CambiarPersistence nueva= new CambiarPersistence();
		em=nueva.getEntityManager(base);
		Conectar conn= new Conectar();
		conn.ConectarBase(base);
		return idaop.getProductoAll(em);
	}
	
	@Override
	public Producto getProductoById(String base,int idProd) throws Exception{
		CambiarPersistence nueva= new CambiarPersistence();
		em=nueva.getEntityManager(base);
		Conectar conn= new Conectar();
		conn.ConectarBase(base);
		return idaop.getProductoXId(em, idProd);
	}
	
	@Override
	public List<Producto> obtenerProductos(String locationURL) throws Exception
	{
		List<Producto> retorno = null;
		try{
			
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				List<Producto> listProdCache = cacheProductos.getProductList(base);
				if(listProdCache != null) 
				{
					retorno = listProdCache;
				}
				else 
				{
					em.clear();
					em = cp.getEntityManager(base);
					Conectar conexion=new Conectar();
					conexion.ConectarBase(base);
					
					retorno = idaop.getProductoAll(em);
					cacheProductos.addProductList(base, retorno);
				}
			}
		}
		catch(Exception e) {
			throw e;
		}
		
		return retorno;
	}
	
	@Override
	public void borrarProducto(Producto prod) throws Exception{
		try {
			idaop.borrarProducto(em, prod);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void actualizarProducto(String base,Producto prod) throws Exception{
		CambiarPersistence nueva= new CambiarPersistence();
		em=nueva.getEntityManager(base);
		try {
			idaop.actualizarProducto(em, prod);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getNombreBaseFromURL(String locationURL)
	{
		String retorno = null;
		try 
		{
			byte[] byteLocation = Base64.getDecoder().decode(locationURL); 
			
			Configuracion config = cacheConfiguracion.getConfigFromCache(new String(byteLocation));
			if(config != null) 
			{
				retorno = config.getNombre();
			}
			else 
			{
				em.clear();
				em = cp.getEntityManager("tucarrito");
				Conectar conexion=new Conectar();
				conexion.ConectarBase("tucarrito");
				
				List<Configuracion> configuracion = iDaoConfiguracion.getConfiguracionUrlFront(em, new String(byteLocation));
				if(!configuracion.isEmpty()) 
				{
					config = configuracion.get(0);
					cacheConfiguracion.addConfigToCache(new String(byteLocation), config);
					retorno = config.getNombre();
				}
			}
		}
		catch(Exception e) {
			
		}
		return retorno;
		
	}
}