package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Marca;

@Local
public interface IEJBMarca {

	void insert(String base, Marca marca);

	List<Marca> getAll(String base);

	Marca getMarcaById(String base, int idMarca);

	Marca getMarcaByNombre(String base, String nombre);

	void actualizar(String base, Marca marca);

	void borrar(String base, Marca marca);

	Marca getMarca(String locationURL, int id) throws Exception;

	List<Marca> getAllMarcas(String locationURL) throws Exception;
	

}
