package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Compra;

public class DaoCompra implements IDaoCompra{

	public DaoCompra() {};
	
	public void insertarCompra(EntityManager em, Compra compra) {
		em.persist(compra);
	}
	
	public List<Compra> getCompraByCliente(EntityManager em, int idCliente,int idCompra) {
		return em.createNamedQuery("Compra.getCompraCliente",Compra.class)
				.setParameter("idCliente", idCliente)
				.setParameter("idCompra", idCompra)
				.getResultList();
	}
	
	public List<Compra> getComprasCliente(EntityManager em, int idCliente){
		return em.createNamedQuery("Compra.getComprasCliente",Compra.class)
				.setParameter("idCliente",idCliente)
				.getResultList();
	}
}
