package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Marca;

public class DAOMarca implements IDAOMarca{

	@Override
	public void insertar(EntityManager em, Marca marca) throws Exception{
		em.persist(marca);
	}
	
	@Override
	public Marca getMarcaById(EntityManager em, int idMarca)throws Exception{
		return em.createNamedQuery("Marca.getMarcaById",Marca.class).setParameter("idmarca", idMarca).getSingleResult();
	}
	
	@Override
	public List<Marca> getMarca(EntityManager em,int id)throws Exception{
		return em.createNamedQuery("Marca.getMarcaById",Marca.class).setParameter("id", id).getResultList();
	}
	
	@Override
	public Marca getMarcaByNombre(EntityManager em, String nombre)throws Exception{
		return em.createNamedQuery("Marca.getMarcaByNombre",Marca.class).setParameter("nombre", nombre).getSingleResult();
	}
	
	@Override
	public List<Marca> getAllMarcas(EntityManager em)throws Exception{
		return em.createNamedQuery("Marca.getAllMarcas",Marca.class).getResultList();
	}
	
	@Override
	public void borrarMarca(EntityManager em, Marca marca) throws Exception{
		System.out.println(marca.getId());
		System.out.println(marca.getNombre());
		
		em.remove(getMarcaById(em,marca.getId()));
	}
	
	@Override
	public void actualizar(EntityManager em, Marca marca) throws Exception{
		em.merge(marca);
	}
}
