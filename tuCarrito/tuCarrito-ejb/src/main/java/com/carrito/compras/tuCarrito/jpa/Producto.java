package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity implementation class for Entity: Producto
 *
 */
@Entity
@NamedQueries({
@NamedQuery(name="Producto.getProductoById",query="SELECT c FROM Producto c WHERE c.id= :idProducto"),
@NamedQuery(name="Producto.getAll",query="SELECT c FROM Producto c"),
@NamedQuery(name="Producto.getProdXMarca",query="SELECT p FROM Producto p WHERE p.marca.id= :idmarca"),
@NamedQuery(name="Producto.getProdXCateg",query="SELECT p FROM Producto p WHERE p.categoria.id= :idcateg")
})
public class Producto implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String nombre;
	
	@NotNull
	@Length(max=255)
	private String descripcion;
	
	@NotNull
	private double precio;
	
	private double descuento;
	
	private double precioConDcto;
	
	@Enumerated(EnumType.STRING)
	private Moneda moneda;
	
	@NotNull
	private int stock;
	
	@NotNull
	private boolean activo;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade= {CascadeType.ALL})
	@JoinColumn(name="marcaId")
	private Marca marca;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade= {CascadeType.ALL})
	@JoinColumn(name="categoriaId")
	private Categoria categoria;
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="idproducto")
	private List<ImagenesProducto> imagenes;
	
	public Producto() { 
		super();
	}

	public Producto(String nombre, String descripcion, double precio, double descuento, double precioConDcto,int stock,
			boolean activo, Marca marca, Categoria categoria, List<ImagenesProducto> imagen,Moneda moneda) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.descuento = descuento;
		this.precioConDcto=precioConDcto;
		this.stock = stock;
		this.activo = activo;
		this.marca = marca;
		this.categoria = categoria;
		this.imagenes = imagen;
		this.moneda=moneda;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Moneda getMoneda() {
		return moneda;
	}

	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}

	public List<ImagenesProducto> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<ImagenesProducto> imagenes) {
		this.imagenes = imagenes;
	}

	public double getPrecioConDcto() {
		return precioConDcto;
	}

	public void setPrecioConDcto(double precioConDcto) {
		this.precioConDcto = precioConDcto;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}
   
	
	
	
}
