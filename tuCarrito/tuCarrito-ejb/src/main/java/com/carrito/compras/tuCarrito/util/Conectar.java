package com.carrito.compras.tuCarrito.util;

import java.sql.*;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Timer;
import java.util.concurrent.Executor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mysql.jdbc.DatabaseMetaData;

public class Conectar {

	private String user="root";
	
	private String password="proyecto2017";
	
	private String base="tucarrito";
	
	private String url="jdbc:mysql://";
	
	private String host="localhost";
	
	private int port=3306;
	
	private String driver="com.mysql.jdbc.Driver";
	
	private Connection conexion=null;

	public Connection ConectarBase(String base)  {
		try {
			String url2=url+host+":"+port+"/"+base+"?verifyServerCertificate=false&useSSL=false";
			Class.forName(driver);
			
			conexion=(Connection)DriverManager.getConnection(url2,user,password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conexion;
	}
	
	public Boolean CrearBase(String base) {
		Boolean creada=false;
		try {
			
			String url2=url+host+":"+port+"/"+"tucarrito"+"?verifyServerCertificate=false&useSSL=false";
			Class.forName(driver);
			conexion=(Connection)DriverManager.getConnection(url2,user,password);
			String query="IF NOT EXIST \r\nCREATE DATABASE "+base;
			
			System.out.println(base);
			Statement prepare=conexion.prepareStatement(query);
			
			prepare.executeUpdate(query);
			
			creada=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return creada;
	}
	
	public Boolean existeBase(String base) {
		
		try {
			System.out.println("Entra en control de si existe");
			
			conexion=ConectarBase("tucarrito");
			ResultSet resultado = conexion.getMetaData().getCatalogs();
			
			System.out.println("Pasa el resultset");
			
			while(resultado.next()) {
				String databasename=resultado.getString(1);
				
				System.out.println("base"+databasename);				
				System.out.println(databasename);
				if (databasename.equals(base.toLowerCase())) {
					return true;
				}
			}
			resultado.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public Boolean existeTabla(String base,String tabla) {
		Boolean retorno=false;
		try {
			System.out.println("Entra en control de si existe la tabla");
			
			conexion=ConectarBase(base);
			DatabaseMetaData metadata=(DatabaseMetaData) conexion.getMetaData();
			ResultSet resultado = metadata.getTables(null, null, "%", null);
			String enbase;
			while(resultado.next()) {
				enbase=resultado.getString(3);
				if(enbase.equals(tabla.toLowerCase())) {
					retorno= true;
				}
			}
			System.out.println("Pasa el resultset");
			resultado.close();
			System.out.println("Se cerro el statement");
			return retorno;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retorno;
	}
	
	public Boolean CrearEstructura(String base) {
		Boolean creada=false;
		try {
		conexion=ConectarBase(base);
		
		String query = "CREATE TABLE `cliente` (\r\n" + 
				"  id int(11) NOT NULL AUTO_INCREMENT,\r\n" + 
				"  imagen tinyblob NOT NULL,\r\n" + 
				"  mail varchar(255) NOT NULL,\r\n" + 
				"  nombre varchar(255) NOT NULL,\r\n" + 
				"  password varchar(255) NOT NULL,\r\n" + 
				"  PRIMARY KEY (id)\r\n" + 
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		String tablacateg="CREATE TABLE categoria(\r\n"+
				  "id int(11) NOT NULL AUTO_INCREMENT,\r\n"+
				  "nombre varchar(255) NOT NULL,\r\n"+
				  "PRIMARY KEY (id)\r\n"+
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		Statement prepare = conexion.prepareStatement(tablacateg);
		
		prepare.executeUpdate(tablacateg);
		creada=true;
		return creada;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return creada;
	}
	public Conectar() {
		super();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
