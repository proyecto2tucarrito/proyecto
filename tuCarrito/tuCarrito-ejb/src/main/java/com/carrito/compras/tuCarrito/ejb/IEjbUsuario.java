package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Usuario;

@Local
public interface IEjbUsuario {
	public void insert(Usuario usuario) throws Exception;
	public Usuario getUsuarioById(int idUsr) throws Exception;
	public void update(Usuario usuario) throws Exception;
	public void delete(int idUsr) throws Exception;
	public void bloquear(Usuario usuario) throws Exception;
	public List<Usuario> getAllUsuario() throws Exception;
	public Usuario iniciarSesion(String email, String password) throws Exception;
	Usuario getUsuarioByEmail(String email);
	boolean existeUsuario(String email);
}
