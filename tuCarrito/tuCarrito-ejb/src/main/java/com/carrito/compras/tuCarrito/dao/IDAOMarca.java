package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Marca;

public interface IDAOMarca {

	void insertar(EntityManager em, Marca marca) throws Exception;

	Marca getMarcaById(EntityManager em, int idMarca)throws Exception;

	void borrarMarca(EntityManager em, Marca marca)throws Exception;

	Marca getMarcaByNombre(EntityManager em, String nombre)throws Exception;

	List<Marca> getAllMarcas(EntityManager em)throws Exception;

	void actualizar(EntityManager em, Marca marca)throws Exception;

	List<Marca> getMarca(EntityManager em, int id) throws Exception;

}
