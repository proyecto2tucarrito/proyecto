package com.carrito.compras.tuCarrito.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * Entity implementation class for Entity: Direccion
 *
 */
@Entity
@Table(name="Direccion")
public class Direccion implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Length(max=255)
	private String calle;
	
	@NotNull
	private String numero;
	
	private String apto;
	
	@NotNull
	@Length(max=255)
	private String pais;
	
	@NotNull
	@Length(max=255)
	private String ciudad;
	
	@NotNull
	@Length(max=255)
	private String departamento;
	
	public Direccion() {
		super();
	}
   
	public Direccion(int id, String calle, String numero, String apto, String pais, String ciudad, String departamento) {
		super();
		this.id = id;
		this.calle = calle;
		this.numero = numero;
		this.apto = apto;
		this.ciudad = ciudad;
		this.departamento = departamento;
		this.pais = pais;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getApto() {
		return apto;
	}

	public void setApto(String apto) {
		this.apto = apto;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	

	
}