package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;

public class DaoConfiguracion implements IDaoConfiguracion {
	
	@Override
	public void insertarConfiguracion(EntityManager em , Configuracion config) throws Exception{
		em.persist(config);
	}

	@Override
	public List<Configuracion> getConfiguracion(EntityManager em, int idUsuario) throws Exception{
		return em.createNamedQuery("Configuracion.getConfiguracion",Configuracion.class)
				.setParameter("idUsuario", idUsuario).getResultList();
	}
	
	@Override
	public void updateConfiguracion(EntityManager em, Configuracion config) throws Exception{
		em.merge(config);
	}
	
	@Override
	public List<Configuracion> getConfiguracionUrlFront(EntityManager em, String url) throws Exception{
		return em.createNamedQuery("Configuracion.getConfiguracionUrlFront",Configuracion.class)
				.setParameter("urlfront", url).getResultList();
	}

	@Override
	public List<Configuracion> getAll(EntityManager em)throws Exception{
		return em.createNamedQuery("Configuracion.getAll",Configuracion.class).getResultList();
	}
	
}
