package com.carrito.compras.tuCarrito.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Categoria;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.jpa.Producto;

public class DaoProducto implements IDaoProducto{

	@Override
	public void crearProducto(EntityManager em,Producto prod, int idCateg,int idMarca) throws Exception{
		IDAOCategoria idaocateg= new DAOCategoria();
		Categoria categ=idaocateg.obtenerCategoriaXId(em, idCateg);
		IDAOMarca idaomarca= new DAOMarca();
		Marca marca= idaomarca.getMarcaById(em, idMarca);
		prod.setCategoria(categ);
		prod.setMarca(marca);
		try {
			em.persist(prod);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Producto getProductoXId(EntityManager em,int idProd) throws Exception{
		return em.createNamedQuery("Producto.getProductoById",Producto.class)
				.setParameter("idProducto", idProd).getSingleResult();
	}
	
	@Override
	public List<Producto> getProductoById(EntityManager em,int id) throws Exception{
	return em.createNamedQuery("Producto.getProductoById",Producto.class)
	.setParameter("idProducto", id).getResultList();
	}

	@Override
	public List<Producto> getProductoAll(EntityManager em) throws Exception{
	return em.createNamedQuery("Producto.getAll", Producto.class).getResultList();
	}

	@Override
	public void actualizarProducto(EntityManager em,Producto prod) throws Exception{
		
		em.merge(prod);
	}

	@Override
	public void borrarProducto(EntityManager em,Producto prod) throws Exception{
	em.remove(prod);
	}
	
	
}
