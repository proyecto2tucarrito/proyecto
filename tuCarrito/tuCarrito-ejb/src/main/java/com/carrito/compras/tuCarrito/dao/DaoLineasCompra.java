package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.LineasCompra;

public class DaoLineasCompra implements IDaoLineasCompra{

	@Override
	public void insertLinea(EntityManager em, LineasCompra linea) {
		em.persist(linea);
	}
	
	@Override 
	public List<LineasCompra> getLineasCompra(EntityManager em, int idCompra){
		return em.createNamedQuery("LineasCompra.getLineas",LineasCompra.class)
				.setParameter("idCompra", idCompra)
				.getResultList();
	}
	
	@Override
	public void deleteLinea(EntityManager em , LineasCompra linea) {
		em.remove(linea);
	}
	
	@Override
	public void updateLineaCompra(EntityManager em, LineasCompra linea) {
		em.merge(linea);
	}
}
