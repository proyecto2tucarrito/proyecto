package com.carrito.compras.tuCarrito.ejb;

import java.util.List;

import javax.ejb.Local;

import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Usuario;

@Local
public interface IEJBConfiguracion {

		public void insertarConfig(Configuracion config,byte[] imagenLogo,byte[] imagenMobile) throws Exception;

		public Configuracion getConfiguracion(Usuario usuario) throws Exception;

		public void updateConfig(Configuracion config) throws Exception;

		public Configuracion getConfiguracionUrlFront(String base, String url) throws Exception;

		List<Configuracion> getAll() throws Exception;
	
}
