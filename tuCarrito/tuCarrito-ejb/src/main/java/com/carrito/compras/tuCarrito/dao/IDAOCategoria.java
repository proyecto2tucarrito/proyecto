package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Categoria;

public interface IDAOCategoria {

	Boolean insetarCategoria(EntityManager em, Categoria categ);

	Categoria buscarCategoriaPorNombre(EntityManager em,String categnombre);

	List<Categoria> obtenerTodas(EntityManager em);

	Categoria obtenerCategoriaXId(EntityManager em, int id);

	void actualizar(EntityManager em, Categoria categ);

	void borrar(EntityManager em, Categoria categ);

}
