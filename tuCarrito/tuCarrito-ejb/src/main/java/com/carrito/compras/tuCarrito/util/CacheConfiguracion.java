package com.carrito.compras.tuCarrito.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;

import com.carrito.compras.tuCarrito.jpa.Configuracion;

@Singleton
public class CacheConfiguracion {

	private Map<String, Configuracion> configuracion = new ConcurrentHashMap<>();

	public void addConfigToCache(String locationURL, Configuracion config) {
		if (!configuracion.containsKey(locationURL)) {
			configuracion.put(locationURL, config);
		}
	}

	public void removeConfigFromCache(String locationURL) {
		if (configuracion.containsKey(locationURL)) {
			configuracion.remove(locationURL);
		}
	}

	public Configuracion getConfigFromCache(String locationURL) {
		if (configuracion.containsKey(locationURL)) {
			return configuracion.get(locationURL);
		} else {
			return null;
		}
	}
}
