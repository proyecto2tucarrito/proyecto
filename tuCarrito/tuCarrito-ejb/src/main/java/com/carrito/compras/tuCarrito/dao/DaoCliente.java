package com.carrito.compras.tuCarrito.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.carrito.compras.tuCarrito.jpa.Cliente;

public class DaoCliente implements IDaoCliente{

	public DaoCliente() {}
	
	@Override
	public void insertarCliente(EntityManager em,Cliente cliente) throws Exception{
		em.persist(cliente);
	}
	
	@Override
	public Cliente getClienteByEmail(EntityManager em, String email) throws Exception{
		return em.createNamedQuery("Cliente.exists",Cliente.class).setParameter("email", email).getSingleResult();
	}
	
	@Override
	public void deleteCliente(EntityManager em, Cliente cliente) throws Exception{
		em.remove(cliente);
	}
	
	@Override
	public void updateCliente(EntityManager em, Cliente cliente) throws Exception{
		em.merge(cliente);
	}
	@Override
	public List<Cliente> getClientesAll(EntityManager em) throws Exception{
		return em.createNamedQuery("Cliente.getAll",Cliente.class).getResultList();
	
	}
	@Override
	public List<Cliente> loginCliente(EntityManager em, String email, String password) throws Exception{
		return em.createNamedQuery("Cliente.login",Cliente.class)
				.setParameter("email", email)
				.setParameter("password", password)
				.getResultList();
	}
	
	@Override
	public List<Cliente> checkEmailExists(EntityManager em, String email) throws Exception{
		return em.createNamedQuery("Cliente.exists",Cliente.class)
				.setParameter("email", email)
				.getResultList();
	}
	
}
