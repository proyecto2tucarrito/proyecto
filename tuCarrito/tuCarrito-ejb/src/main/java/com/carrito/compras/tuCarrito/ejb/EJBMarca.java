package com.carrito.compras.tuCarrito.ejb;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carrito.compras.tuCarrito.dao.DAOMarca;
import com.carrito.compras.tuCarrito.dao.DaoConfiguracion;
import com.carrito.compras.tuCarrito.dao.IDAOMarca;
import com.carrito.compras.tuCarrito.dao.IDaoConfiguracion;
import com.carrito.compras.tuCarrito.jpa.Configuracion;
import com.carrito.compras.tuCarrito.jpa.Marca;
import com.carrito.compras.tuCarrito.util.CacheConfiguracion;
import com.carrito.compras.tuCarrito.util.CambiarPersistence;
import com.carrito.compras.tuCarrito.util.Conectar;

@Stateless
@LocalBean
public class EJBMarca implements IEJBMarca{
	
	@PersistenceContext
	private EntityManager em=null;
	
	@EJB
	private CacheConfiguracion cacheConfiguracion;
	private CambiarPersistence cp = new CambiarPersistence();
	
	private IDAOMarca iDaoMarca = new DAOMarca();
	private IDaoConfiguracion iDaoConfiguracion = new DaoConfiguracion();
	
	IDAOMarca idaomarca= new DAOMarca();
	
	@Override
	public void insert(String base,Marca marca) {
		try {
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
			idaomarca.insertar(em, marca);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Marca> getAll(String base){
		List<Marca> lmarcas= new ArrayList<Marca>();
		try {
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
			lmarcas=idaomarca.getAllMarcas(em);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return lmarcas;
	}
	
	@Override
	public Marca getMarcaById(String base,int idMarca){
		Marca marca=null;
		try {
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
			System.out.print(base);
			System.out.println(idMarca);
			marca= idaomarca.getMarcaById(em, idMarca);
			System.out.println(marca.getNombre());
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return marca;
	}
	
	@Override
	public Marca getMarcaByNombre(String base,String nombre) {
		try {
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
			return idaomarca.getMarcaByNombre(em, nombre);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void actualizar(String base, Marca marca) {
		
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
		try {
			
			idaomarca.actualizar(em, marca);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void borrar(String base,Marca marca) {
		
			CambiarPersistence nueva= new CambiarPersistence();
			em=nueva.getEntityManager(base);
		try {
			
			idaomarca.borrarMarca(em, marca);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Marca getMarca(String locationURL, int id) throws Exception{
		Marca marca = null;
		try 
		{
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				List<Marca> listMarca = iDaoMarca.getMarca(em, id);
				if(!listMarca.isEmpty()) {
					marca=listMarca.get(0);
				}
			}
		}
		catch(Exception e) {
			throw e;
		}
		return marca;
	}
	
	@Override
	public List<Marca> getAllMarcas(String locationURL) throws Exception{
		try{
			
			String base = getNombreBaseFromURL(locationURL);
			if(base != null) 
			{
				em.clear();
				em = cp.getEntityManager(base);
				Conectar conexion=new Conectar();
				conexion.ConectarBase(base);
				
				return iDaoMarca.getAllMarcas(em);
			}
			else
			{
				return null;
			}
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	private String getNombreBaseFromURL(String locationURL)
	{
		String retorno = null;
		try 
		{
			byte[] byteLocation = Base64.getDecoder().decode(locationURL); 
			
			Configuracion config = cacheConfiguracion.getConfigFromCache(new String(byteLocation));
			if(config != null) 
			{
				retorno = config.getNombre();
			}
			else 
			{
				em.clear();
				em = cp.getEntityManager("tucarrito");
				Conectar conexion=new Conectar();
				conexion.ConectarBase("tucarrito");
				
				List<Configuracion> configuracion = iDaoConfiguracion.getConfiguracionUrlFront(em, new String(byteLocation));
				if(!configuracion.isEmpty()) 
				{
					config = configuracion.get(0);
					cacheConfiguracion.addConfigToCache(new String(byteLocation), config);
					retorno = config.getNombre();
				}
			}
		}
		catch(Exception e) {
			
		}
		return retorno;
		
	}
}
